<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesBill;
use App\Models\SalesPayment;
use App\Models\OldPay;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class SalesPaymentController extends Controller
{
    public function index(){
    	return view('payment.sales');
    }

    public function oldpayments(){
    	return view('payment.oldpayments');
    }

    public function billno(Request $request){
    	
		$json=array();

		$status = SalesBill::where('bill_no','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);						
	}

	public function billdetails(Request $request){
		$bill = SalesBill::where('bill_no',[$request->value])
				->get();
		
		$tot = 0;

		foreach ($bill as $key => $value) {
				$cutname = DB::table('customers')
						->select('name')
						->where('id', $value->custid)	
						->get();

				$tot +=  $value->total;	
				$json['amountpay'] =  $value->amountpayed;
				$json['amountpayl'] =  $value->amountpay;
				$json['custid'] = $value->custid;
			}

		$json['custname'] =  $cutname[0]->name;
		$json['total'] =  round($tot);

		echo json_encode($json);
	}

	public function billadd(Request $request){

		$status = SalesPayment::where('billno',$request->psbillno)
						->where('billdate',date('y-m-d'))
						->get();

		$idd = 0;
		foreach ($status as $key => $value) {
			$idd = $value->id;
		}

		if($idd == " "){
			// echo "sd";die;
			$salespayment = new SalesPayment();
			$salespayment->billno = $request->psbillno;
			$salespayment->desc = "Payment";
			$salespayment->billdate = date('y-m-d');
			$salespayment->amount = $request->psalespay;
			$salespayment->mode = $request->spmode;
			$salespayment->ref = $request->spref;
			$salespayment->save ();
		} else {
			// echo $idd."ds";die;
			DB::update("UPDATE sales_payments SET amount = amount + '$request->psalespay' where id = '$idd'");
		}		
   	

		DB::update("UPDATE sales_bills SET amountpay = amountpay + '$request->psalespay' where bill_no = '$request->psbillno'");

		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 2");
        DB::update("UPDATE accounts SET amount = amount - '$request->psalespay' where id = 5");
		DB::update("UPDATE customers SET amount = amount - '$request->psalespay' where id = '$request->pcustid'");

    	return redirect('home');
    	   	
    }


    public function customer(){
    	return view('payment.customer');
    }


    public function custbilladd(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate;die;
		$salespayment = new SalesPayment();
		$salespayment->billno = $request->pcustid;
		$salespayment->desc = "Customer Payment";
		$salespayment->billdate = date('y-m-d');
		$salespayment->amount = $request->psalespay;
		$salespayment->mode = $request->scpmode;
		$salespayment->ref = $request->scpref;
		$salespayment->save ();


		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 2");
		DB::update("UPDATE accounts SET amount = amount - '$request->psalespay' where id = 5");

		DB::update("UPDATE customers SET amount = amount - '$request->psalespay' where id = '$request->pcustid'");

    	return redirect('home');
    	   	
    }

    public function customerreportdetails(Request $request){
		// echo $request->dailydate;die;
		$value = $request->value;

		// echo $value;die;



		$sales_bills = DB::table('sales_bills')
							->select('*')
							->where('custid',$value)
							->get();
											
		$sales_bills_payments = DB::table('sales_payments')
							->select('*')
							->where('billno',$value)
							->where('desc','Customer Payment')
							->get();

		// echo print_r($sales_bills_payments);die;
		return view('payment.customerreportlist', compact('value', 'sales_bills_payments', 'sales_bills'));
	}


	public function old(){
		$oldpays = DB::table('old_pays')
						->select('*')
						->get();
    	return view('payment.old', compact('oldpays'));
	}

	public function oldadd(Request $request){

		$oldpay = new OldPay();
		$oldpay->oname = $request->oname;
		$oldpay->oitem = $request->oitem;
		$oldpay->oamount = $request->oamount;
		$oldpay->odate = date('Y-m-d');
		
		$oldpay->save ();


		DB::update("UPDATE accounts SET amount = amount + '$request->oamount' where id = 1");
    	// return response()->json(['success'=>'Data is successfully added']); 
    	return redirect('payment/old');
    	   	
    }
}
