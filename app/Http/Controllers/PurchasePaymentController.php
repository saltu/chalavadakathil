<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PurchaseBill;
use App\Models\PurchasePayment;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class PurchasePaymentController extends Controller
{
	public function index(){
    	return view('payment.purchase');
    }

    public function billno(Request $request){
    	
		$json=array();

		$status = PurchaseBill::where('bill_no','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);						
	}

	public function billdetails(Request $request){
		$bill = PurchaseBill::where('bill_no',[$request->value])
				->get();
		
		$tot = 0;

		foreach ($bill as $key => $value) {
				$purname = DB::table('purchasers')
						->select('name')
						->where('id', $value->purid)	
						->get();

				$tot +=  $value->total;	
				$json['amountpay'] =  $value->amountpayed;
				$json['amountpayl'] =  $value->amountpay;
				$json['ppurid'] = $value->purid;
			}

		$json['ppurname'] =  $purname[0]->name;
		$json['ppurchasetot'] =  round($tot);

		echo json_encode($json);
	}

	public function billadd(Request $request){

		$purchasepayment = new PurchasePayment();
		$purchasepayment->billno = $request->ppbillno;
		$purchasepayment->desc = "Payment";
		$purchasepayment->billdate = date('y-m-d');
		$purchasepayment->amount = $request->ppurchasepay;
		$purchasepayment->mode = $request->ppmode;
		$purchasepayment->ref = $request->ppref;
		$purchasepayment->save ();


		DB::update("UPDATE purchase_bills SET amountpay = amountpay + '$request->ppurchasepay' where bill_no = '$request->ppbillno'");

		DB::update("UPDATE accounts SET amount = amount - '$request->ppurchasepay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->ppurchasepay' where id = 3");
        DB::update("UPDATE accounts SET amount = amount - '$request->ppurchasepay' where id = 6");
		DB::update("UPDATE purchasers SET amount = amount - '$request->ppurchasepay' where id = '$request->ppurid'");

    	return redirect('home');
    	   	
    }

    public function purchaser(){
    	return view('payment.purchaser');
    }

     public function purbilladd(Request $request){

		$purchasepayment = new PurchasePayment();
		$purchasepayment->billno = $request->ppurid;
		$purchasepayment->desc = "Purchaser Payment";
		$purchasepayment->billdate = date('y-m-d');
		$purchasepayment->amount = $request->ppurchasepay;
		$purchasepayment->mode = $request->pppmode;
		$purchasepayment->ref = $request->pppref;
		$purchasepayment->save ();


		DB::update("UPDATE accounts SET amount = amount - '$request->ppurchasepay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->ppurchasepay' where id = 3");
        DB::update("UPDATE accounts SET amount = amount - '$request->ppurchasepay' where id = 6");
		DB::update("UPDATE purchasers SET amount = amount - '$request->ppurchasepay' where id = '$request->ppurid'");

    	return redirect('home');
    	   	
    }


    public function purchaserreportdetails(Request $request){
		$value = $request->value;


		$purchases_bills = DB::table('purchase_bills')
							->select('*')
							->where('purid',$value)
							->get();
											
		$purchases_bills_payments = DB::table('purchase_payments')
							->select('*')
							->where('billno',$value)
							->where('desc','Purchaser Payment')
							->get();

		return view('payment.purchaserreportlist', compact('value', 'purchases_bills_payments', 'purchases_bills'));
	}


}
