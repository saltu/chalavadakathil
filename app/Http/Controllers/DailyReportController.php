<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class DailyReportController extends Controller
{
	public function index(){
		return view('dailyreport.report');
	}

	public function reportdetails(Request $request){
		$dailydate = $request->dailydate;


		$opening = DB::table('open_closes')
						->select('*')
						->where('current_date',$dailydate)
						->get();

		$sales_bills = DB::table('sales_payments')
							->select('*')
							->where('billdate',$dailydate)
							->where('desc','!=','Customer Payment')
							->get();
		$sales_bills_cus = DB::table('sales_payments')
							->select('*')
							->where('billdate',$dailydate)
							->where('desc','Customer Payment')
							->get();

		$purchase_bills = DB::table('purchase_payments')
							->select('*')
							->where('billdate',$dailydate)
							->where('desc','!=','Purchaser Payment')
							->where('desc','=','Bill')
							->get();
		$purchase_bills_pur = DB::table('purchase_payments')
							->select('*')
							->where('billdate',$dailydate)
							->where('desc','Purchaser Payment')
							->get();

		$stock_details = DB::table('stock_details')
							->select('*')
							->where('cudate',$dailydate)
							->orderBy('id','ASC')
							->get();
		$expenses = DB::table('expenses')
							->select('*')
							->where('ex_date',$dailydate)
							->orderBy('id','DESC')
							->get();
		$loan_details_cr = DB::table('loan_details')
							->select('*')
							->where('lddate',$dailydate)
							->where('type','Cr')
							->orderBy('id','DESC')
							->get();
		$loan_details_dr = DB::table('loan_details')
							->select('*')
							->where('lddate',$dailydate)
							->where('type','Dr')
							->orderBy('id','DESC')
							->get();
		$old_payments = DB::table('old_pays')
							->select('*')
							->where('odate',$dailydate)
							->get();
		$rentals = DB::table('rentals')
							->select('*')
							->where('rdate',$dailydate)
							->get();

		return view('dailyreport.reportlist', compact('opening', 'sales_bills','purchase_bills', 'stock_details' ,'expenses', 'loan_details_cr' , 'loan_details_dr','sales_bills_cus','purchase_bills_pur','old_payments','rentals'));
	}

	public function creport(){
		return view('dailyreport.creport');
	}

	public function creportdetails(Request $request){
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		$opening = DB::table('open_closes')
						->select('*')
						->whereBetween('current_date',[$datefrom,$dateto])
						->orderBy('current_date','ASC')
						->get();

		$sales_bills = DB::table('sales_payments')
							->select('*')
							->whereBetween('billdate',[$datefrom,$dateto])
							->where('desc','!=','Customer Payment')
							->get();
		$sales_bills_cus = DB::table('sales_payments')
							->select('*')
							->whereBetween('billdate',[$datefrom,$dateto])
							->where('desc','Customer Payment')
							->get();

		$purchase_bills = DB::table('purchase_payments')
							->select('*')
							->whereBetween('billdate',[$datefrom,$dateto])
							->where('desc','!=','Purchaser Payment')
							->where('desc','=','Bill')
							->get();
		$purchase_bills_pur = DB::table('purchase_payments')
							->select('*')
							->whereBetween('billdate',[$datefrom,$dateto])
							->where('desc','Purchaser Payment')
							->get();

		$stock_details = DB::table('stock_details')
							->select('*')
							->whereBetween('cudate',[$datefrom,$dateto])
							->orderBy('id','ASC')
							->get();
		$expenses = DB::table('expenses')
							->select('*')
							->whereBetween('ex_date',[$datefrom,$dateto])
							->orderBy('id','DESC')
							->get();
		$loan_details_cr = DB::table('loan_details')
							->select('*')
							->whereBetween('lddate',[$datefrom,$dateto])
							->where('type','Cr')
							->orderBy('id','DESC')
							->get();
		$loan_details_dr = DB::table('loan_details')
							->select('*')
							->whereBetween('lddate',[$datefrom,$dateto])
							->where('type','Dr')
							->orderBy('id','DESC')
							->get();
		$old_payments = DB::table('old_pays')
							->select('*')
							->whereBetween('odate',[$datefrom,$dateto])
							->get();
		$rentals = DB::table('rentals')
							->select('*')
							->whereBetween('rdate',[$datefrom,$dateto])
							->get();

		return view('dailyreport.creportlist', compact('opening', 'sales_bills','purchase_bills','stock_details','expenses', 'loan_details_cr' , 'loan_details_dr','sales_bills_cus','purchase_bills_pur','old_payments','rentals'));
	}

	
}
