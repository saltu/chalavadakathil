<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class ExpenseController extends Controller
{
    public function index(){
    	$expenses1 = DB::table('expenses')
						->select('*')
						->where('type','Dr')
						->get();
						
		$expenses2 = DB::table('expenses')
						->select('*')
						->where('type','Cr')
						->get();

    	return view('expense.list', compact('expenses1', 'expenses2'));
    }

    public function add(Request $request){

		if (Expense::where('id', $request->exid)->count() > 0) {
			DB::update("UPDATE expenses SET description = '$request->exdescrip', amount = '$request->examount' WHERE id = ? ",[$request->exid]);

    		return redirect('expense');
		} else {
			$expense = new Expense();
			$expense->description = $request->exdescrip;
			$expense->amount = $request->examount;
			$expense->ex_date = date('Y-m-d');
			$expense->type = $request->type;
			
			$expense->save ();

			DB::update("UPDATE accounts SET amount = amount - '$request->examount' where id = 1");
			DB::update("UPDATE accounts SET amount = amount + '$request->examount' where id = 4");
	    	

	    	return redirect('expense');
		}
    	   	
    }

    public function edit($id){
    	$expenses = DB::table('expenses')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('expense.edit', compact('expenses'));
    }

     public function delete($id){

     	$expenses = DB::table('expenses')
						->select('*')
						->where('id' , $id)
						->get();

		$amoun = $expenses[0]->amount;
		
		DB::update("UPDATE accounts SET amount = amount + '$amoun' where id = 1");
		DB::update("UPDATE accounts SET amount = amount - '$amoun' where id = 4");

		$expense = Expense::findorfail($id);
		$expense->destroy($id);

		return redirect('expense');  	
    }
}
