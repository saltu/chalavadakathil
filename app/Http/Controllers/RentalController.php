<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rental;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class RentalController extends Controller
{
    public function index(){
		$rentals = DB::table('rentals')
						->select('*')
						->get();
    	return view('rental.list', compact('rentals'));
	}

	public function add(Request $request){

		$rental = new Rental();
		$rental->rdesc = $request->rdesc;
		$rental->ramount = $request->ramount;
		$rental->rdate = date('Y-m-d');
		
		$rental->save ();


		DB::update("UPDATE accounts SET amount = amount + '$request->ramount' where id = 1");
    	return redirect('rental');
    	   	
    }
}
