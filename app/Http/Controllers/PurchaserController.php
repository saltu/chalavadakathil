<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchaser;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class PurchaserController extends Controller
{
    public function index(){
    	$purchasers = DB::table('purchasers')
						->select('*')
						->get();
    	return view('purchaser.list', compact('purchasers'));
    }

    public function add(Request $request){


		if (Purchaser::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE purchasers SET name = '$request->name', phone = '$request->phone' WHERE id = ? ",[$request->id]);
    		return redirect('purchaser'); 
		} else {
			$purchaser = new Purchaser();
			$purchaser->name = $request->name;
			$purchaser->phone = $request->phone;
			$purchaser->amount = 0;
			$purchaser->save ();
	    	return redirect('purchaser'); 
		}
    	   	
    }

  	public function edit($id){
    	$purchasers = DB::table('purchasers')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('purchaser.edit', compact('purchasers'));
    }


     public function delete($id){

		$purchasers = Purchaser::findorfail($id);
		$purchasers->destroy($id);
 
		return redirect('purchaser');  	
    }

    public function purname(Request $request){
    	
		$json=array();

		$status = Purchaser::where('name','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->name;
			}

		echo json_encode($json);						
	}

	public function purdetails(Request $request){
		$pur = Purchaser::where('name',[$request->value])
				->get();

		foreach ($pur as $key => $value) {
				$json['id'] =  $value->id;		
				$json['phone'] =  $value->phone;
				$json['amount'] =  $value->amount;
			}

		echo json_encode($json);

	}
}
