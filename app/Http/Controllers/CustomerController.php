<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


class CustomerController extends Controller
{
    public function index(){
    	$customers = DB::table('customers')
						->select('*')
						->get();
    	return view('customer.list', compact('customers'));
    }

    public function add(Request $request){

		if (Customer::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE customers SET name = '$request->name', phone = '$request->phone' WHERE id = ? ",[$request->id]);
    		return redirect('customer');
		} else {

			if(Customer::where('name', $request->name)->count() > 0){
				return Redirect::back()->withInput(Input::all())->withErrors(['Please enter another name']);
			} else {
				$customer = new Customer();
				$customer->name = $request->name;
				$customer->phone = $request->phone;
				$customer->amount = 0;
				
				$customer->save ();
		    	return redirect('customer');
		    }
		}	   	
    }

    public function edit($id){
    	$customers = DB::table('customers')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('customer.edit', compact('customers'));
    }

     public function delete($id){

		$customers = Customer::findorfail($id);
		$customers->destroy($id);

		return redirect('customer');
    }

    public function custname(Request $request){
    	
		$json=array();

		$status = Customer::where('name','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->name;
			}

		echo json_encode($json);						
	}

	public function name(Request $request){
    	
		$json=array();

		$status = Customer::where('name', [$request->name])
						->get();

		if (count($status) > 0){
			$json['count'] = '1';
		}else{
			$json['count'] = '0';
		}

		echo json_encode($json);						
	}

	public function custdetails(Request $request){
		$cust = Customer::where('name',[$request->value])
				->get();

		foreach ($cust as $key => $value) {
				$json['id'] =  $value->id;		
				$json['phone'] =  $value->phone;
				$json['amount'] = $value->amount;
			}

		echo json_encode($json);

	}
    
}
