<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\VehiclesMaintain;
use App\Models\Expense;
use App\Models\Notification;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class VehicleController extends Controller
{
    public function index(){
    	$vehicles = DB::table('vehicles')
						->select('*')
						->where('ownername','Chalavadakathil')
						->get();

		$vehicleess = DB::table('vehicles')
						->select('*')
						->where('ownername','!=','Chalavadakathil')
						->get();
    	return view('vehicle.list', compact('vehicles','vehicleess'));
    }

    public function ntf_view(Request $request){
    	
		$json=array();

		$status = Notification::where('view', [$request->view])
							->get();
		foreach ($status as $key => $value) {
			DB::table('notifications')
            	->where('id',$value->id)
            	->update(['view' => 1]);
		}
		$json['count'] = '1';

		echo json_encode($json);						
	}

    public function add(Request $request){


		if (Vehicle::where('id', $request->id)->count() > 0) {
			$idn = $request->id;
			DB::update("UPDATE vehicles SET number = '$request->number', type = '$request->type', capacity = '$request->capacity', descr = '$request->desc', km = '$request->km', permit = '$request->permit', tax = '$request->tax', test = '$request->test', insurance = '$request->insurance', ownername = '$request->ownername' WHERE id = ? ",[$request->id]);

			if (Notification::where('id',$idn)->count() > 0){
				$notifyd = Notification::findorfail($idn);
				$notifyd->destroy($idn);
			}
 
    		return redirect('vehicle');
		} else {
			$vehicle = new Vehicle();
			$vehicle->number = $request->number;
			$vehicle->type = $request->type;
			$vehicle->capacity = $request->capacity;
			$vehicle->descr = $request->desc;
			$vehicle->km = $request->km;
			$vehicle->permit = $request->permit;
			$vehicle->tax = $request->tax;
			$vehicle->test = $request->test;
			$vehicle->insurance = $request->insurance;
			$vehicle->ownername = $request->ownername;
			$vehicle->save ();
			
	    	return redirect('vehicle');
		}
    	   	
    }

    public function edit($id){
    	$vehicles = DB::table('vehicles')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('vehicle.edit', compact('vehicles'));
    }

     public function delete($id){

		$vehicles = Vehicle::findorfail($id);
		$vehicles->destroy($id);

		return redirect('vehicle');
    }

    public function dates(){
    	$vehicles = DB::table('vehicles')
						->select('*')
						->where('ownername','Chalavadakathil')
						->get();
    	return view('vehicle.vehicle_date', compact('vehicles'));
    }

	public function maintain(){
    	$vehicle_maintains = DB::table('vehicles_maintains')
						->select('*')
						->get();
    	return view('vehicle.vehicle_maintain', compact('vehicle_maintains'));
    }

    public function madd(Request $request){

		if (VehiclesMaintain::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE vehicles_maintains SET vehicle_id = '$request->vehicle_id', descrp = '$request->descrp', amount = '$request->amount' WHERE id = ? ",[$request->id]);
    		return redirect('vehicle/maintain');
		} else {
			$vehicle = new VehiclesMaintain();
			$vehicle->mdate = date('Y-m-d');
			$vehicle->vehicle_id = $request->vehicle_id;
			$vehicle->descrp = $request->descrp;
			$vehicle->amount = $request->amount;

			$vehicle->save ();


			$expense = new Expense();
			$expense->ex_date = date('Y-m-d');
			$expense->description = "Vehicle maintains";
			$expense->amount = $request->amount;

			$expense->save ();
			
			DB::update("UPDATE accounts SET amount = amount - '$request->amount' where id = 1");
			DB::update("UPDATE accounts SET amount = amount + '$request->amount' where id = 4");

	    	return redirect('vehicle/maintain');
		}
    	   	
    }

    public function medit($id){
    	$vehicles = DB::table('vehicles_maintains')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('vehicle.medit', compact('vehicles'));
    }

     public function mdelete($id){

		$vehiclesmaintain = VehiclesMaintain::findorfail($id);
		$vehiclesmaintain->destroy($id);

		return redirect('vehicle/maintain');
    }

    public function vehiclenum(Request $request){
    	
		$json=array();

		$status = Vehicle::where('number','like', '%' . $request->term . '%')
						->where('ownername','Chalavadakathil')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->number;
			}

		echo json_encode($json);						
	}

	public function vehicledetails(Request $request){
		$veh = Vehicle::where('number',[$request->value])
						->where('ownername','Chalavadakathil')
				->get();

		foreach ($veh as $key => $value) {
				$json['id'] =  $value->id;		
			}

		echo json_encode($json);

	}

	public function vehiclenumm(Request $request){
    	
		$json=array();

		$status = Vehicle::where('number','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->number;
			}

		echo json_encode($json);						
	}

	public function vehicledetailss(Request $request){
		$veh = Vehicle::where('number',[$request->value])
						->get();

		foreach ($veh as $key => $value) {
				$json['id'] =  $value->id;
				$json['capacity'] =  $value->capacity;	
			}

		echo json_encode($json);

	}
}
