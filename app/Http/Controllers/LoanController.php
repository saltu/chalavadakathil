<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanDetails;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class LoanController extends Controller
{
     public function index(){
    	$loans = DB::table('loans')
						->select('*')
						->get();
    	return view('loan.list', compact('loans'));
    }

    public function add(Request $request){

		if (Loan::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE loans SET pamount = pamount + '$request->bamount' WHERE id = ? ",[$request->id]);

			$loandetail = new LoanDetails();
			$loandetail->lddate = date('Y-m-d');
			$loandetail->source_id = $request->id;
			$loandetail->amount = $request->bamount;
			$loandetail->type = "Dr";
			
			$loandetail->save ();
			DB::update("UPDATE accounts SET amount = amount - '$request->bamount' where id = 1");
			DB::update("UPDATE accounts SET amount = amount + '$request->bamount' where id = 4");
			DB::update("UPDATE accounts SET amount = amount - '$request->bamount' where id = 7");
			DB::update("UPDATE accounts SET amount = amount + '$request->bamount' where id = 8");
    		return redirect('loan');
		} else {
			$loan = new Loan();
			$loan->ldate = date('Y-m-d');
			$loan->source = $request->source;
			$loan->lamount = $request->lamount;
			$loan->pamount = 0;
			
			$loan->save ();
			DB::update("UPDATE accounts SET amount = amount + '$request->lamount' where id = 1");

			$loandetail = new LoanDetails();
			$loandetail->lddate = date('Y-m-d');
			$loandetail->source_id = $loan->id;
			$loandetail->amount = $request->lamount;
			$loandetail->type = "Cr";
			
			$loandetail->save ();
			DB::update("UPDATE accounts SET amount = amount + '$request->lamount' where id = 7");
	    	return redirect('loan');
		}
    	   	
    }

    public function edit($id){
    	$loans = DB::table('loans')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('loan.edit', compact('loans'));
    }

    public function report(){
    	return view('loan.report');
    }

    public function sourcename(Request $request){
    	$json=array();

		$status = Loan::where('source','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->source;
			}

		echo json_encode($json);
    }

    public function reportdata(Request $request){
		$source = $request->value;

		$loan_datas = DB::table('loans')
						->select('*')
						->where('source',$source)
						->orderBy('id','DESC')
						->get();
		$loan_data_details = DB::table('loan_details')
						->select('*')
						->where('source_id',$loan_datas[0]->id)
						->get();


		return view('loan.reportlist', compact('loan_data_details'));
	}

}
