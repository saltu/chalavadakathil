<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\OpenClose;
use App\Models\StockDetails;
use App\Models\Notification;
use DB;

class HomeController extends Controller
{
	
    public function index(){
    	if (!Auth::check()) {
		     return redirect('/');
		}		

		$date = date('Y-m-d');
	    $status = 0;
	    $amount = 0;
	    $acc = DB::table ('accounts')->select ('*')->where ('id', 1)->get ();
	    
	    $amount = $acc[0]->amount;
	    // foreach ($acc as $key => $value) {
	    //     $amount = $value->amount;
	    // }

	    $predate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($date)). " +1 month"));
	    $notify = DB::table ('vehicles')->select ('*')->where ('ownername','=','chalavadakathil' )->get();
	    foreach ($notify as $key => $value) {
	    		$eid =$value->id;
	    		$vehicle_id =$value->number;
			    $epermit = $value->permit;
			    $etax = $value->tax;
			    $etest = $value->test;
			    $einsurance = $value->insurance;
			    $vid = 0;
			    $notificationss = DB::table ('notifications')
	    				->select ('*')->where ('id','=',$eid )
	    				->get();
	    		foreach ($notificationss as $key => $value) {
	    			$vid = $value->id;
	    		}
	    	if($predate == $epermit || $predate == $etax || $predate == $etest || $predate == $einsurance)
	    	{
	    		if($vid != $eid){
		    		$notification = new Notification;
		    		$notification->id =$eid;
		    		$notification->vehicle_id =$vehicle_id;
				    $notification->epermit = $epermit;
				    $notification->etax = $etax;
				    $notification->etest = $etest;
				    $notification->einsurance = $einsurance;
					$notification->save();
				}
			}	    	    		    		    	
	    }

	    $acc1 = DB::table ('open_closes')->select ('*')->where ('current_date', $date)->get ();

	    $status = $acc1[0]->id;

	    // foreach ($acc1 as $key => $value) {
     //    	$status = $value->id;
    	// }

	    if($status == 0)
	    {
	        $open = new OpenClose();
	        $open->current_date = $date;
	        $open->opening = $amount;
	        $open->save();
	    }

	    $status1 = 0;
	    $stock = DB::table ('products')->select ('*')->get ();
	    
	    
	    $stock1 = DB::table ('stock_details')->select ('*')->where ('cudate', $date)->get ();

	    foreach ($stock1 as $key => $value) {
	            $status1 = $value->id;
	        }

	    if($status1 == 0)
	    {
	    	foreach ($stock as $key => $value) {
	        	$stock = new StockDetails();
		        $stock->material_id = $value->id;
		        $stock->cft = $value->cft;
		        $stock->cudate = $date;
		        $stock->save();
	    	}
	        
	    }
    	return view('home');
    }
    private $exported_database;
    public function backup()
    {           
                $path = "db_backup/";
        
                $this->connect("localhost","root","","chalavadakathil");
                $this->backupdb();
                if($this->save($path))
                {
                     echo ('<script type="text/javascript">alert("Backup Saved Successfully");</script>');
                     echo ('<script>location.href="home"</script>');
		        }
                else
                {
                    echo("Error in file path");
                }
    }
    
    function connect($server,$user,$pass,$db)
    {       
			$conn= @mysql_connect($server,$user,$pass);
			$condb=@mysql_select_db($db);
			if(!$conn || !$condb)
            {
				echo mysql_error();
			}
            else
            {
                return true;
			}
    }
    	
	function backupdb()
    {
			$table_sql = array();
			foreach ($this->tables() as $key => $table) {
				$tbl_query=mysql_query("SHOW CREATE TABLE ".$table);
				$row2 = mysql_fetch_row($tbl_query);
				$table_sql[]=$row2[1];
			}
			
			$solid_tablecreate_sql = implode("; \n\n" , $table_sql);

			$all_table_data=array();
			foreach ($this->tables() as $key => $table) {
				$show_field=$this->view_fields($table);
				$solid_field_name=implode(", ",$show_field);
				$create_field_sql="INSERT INTO `$table` ( ".$solid_field_name.") VALUES \n";

				//Start checking data available
				mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
				$table_data= mysql_query("SELECT*FROM ".$table);
				if(!$table_data){
					echo 'Could not run query: '. mysql_error();
				}
				
				if (mysql_num_rows($table_data) > 0) {
					$data_viewig=$this->view_data($table);
					$splice_data = array_chunk($data_viewig,50);
					foreach($splice_data as $each_datas){
						$solid_data_viewig=implode(", \n",$each_datas)."; ";
						$all_table_data[]=$create_field_sql.$solid_data_viewig;
					}
				
				}
				else{
					$all_table_data[]=null;
				}	
			}
			$entiar_table_data=implode(" \n\n\n",$all_table_data);
			$this->exported_database=$solid_tablecreate_sql."; \n \n".$entiar_table_data;
			return $this;
		}
    
    function tables(){
			$tb_name=mysql_query("SHOW TABLES");
			$tables=array();
			while ($tb=mysql_fetch_row($tb_name)) {
				$tables[]=$tb[0] ;
			}
			return $tables;
    }
    function view_fields($tablename){
			$all_fields=array();
			$fields = mysql_query("SHOW COLUMNS FROM ".$tablename);
			if (!$fields) {
			 echo 'Could not run query: ' . mysql_error();
			}
			
			if (mysql_num_rows($fields) > 0) {
				while ($field = mysql_fetch_assoc($fields)) {
					$all_fields[]="`".$field["Field"]."`";
				}
			}
			return $all_fields;
	}
	function view_data($tablename){
			$all_data=array();
			$table_data=mysql_query("SELECT*FROM ".$tablename);
			if(!$table_data){
				echo 'Could not run query: '. mysql_error();
			}

			if(mysql_num_rows($table_data)>0){

				
				while ($t_data=mysql_fetch_row($table_data)) {

					$per_data=array();
					foreach ($t_data as $key => $tb_data) {
						$per_data[]= "'".str_replace("'","\'",$tb_data)."'";
					}
					$solid_data= "(".implode(", ",$per_data).")";
					$all_data[]=$solid_data;
				}
			}
				return $all_data;
	}
    function save($path,$name=""){
			$name = ($name != "") ? $name : 'backup_' . date('Y-m-d');
			
			//Save file
			$file = fopen($path.$name.".sql","w+");
			$fw = fwrite($file, $this->exported_database);	
			if(!$fw){
				return false;
			}
			else {
				return true; 
			}
	}
}
