<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class StocksController extends Controller
{
    public function index(){
    	$products = DB::table('products')
						->select('*')
						->get();
    	return view('stock.list', compact('products'));
    }

    public function add(Request $request){

		if (Product::where('id', $request->id)->count() > 0) {

			DB::update("UPDATE products SET name = '$request->name', cft = '$request->cft' WHERE id = ? ",[$request->id]);


    		return redirect('stock');
		} else {
			$product = new Product();
			$product->name = $request->name;
			$product->cft = $request->cft;
			
			$product->save ();

	    	return redirect('stock');
		}
    	   	
    }

	public function edit($id){
    	$products = DB::table('products')
						->select('*')
						->where('id' , $id)
						->get();
    	return view('stock.edit', compact('products'));
    }

     public function delete($id){

		$product = Product::findorfail($id);
		$product->destroy($id);

		return redirect('stock');  	
    }

    public function prodname(Request $request){
    	
		$json=array();

		$status = Product::where('name','like', '%' . $request->term . '%')						
						
						->get();

		foreach ($status as $key => $value) {
				$val = $value->name;
				$json[] =  $val;
			}

		echo json_encode($json);						
	}

	public function proddetails(Request $request){

		$cust = Product::where('name',[$request->value])
				->get();

		foreach ($cust as $key => $value) {
				$json['id'] =  $value->id;
				$json['cft'] =  $value->cft;

			}

		echo json_encode($json);

	}
}
