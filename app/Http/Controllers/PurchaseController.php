<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\InvoiceNum;
use App\Models\Purchaser;
use App\Models\PurchaseBill;
use App\Models\PurchasePayment;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class PurchaseController extends Controller
{
    public function index(){
    	$invoice = DB::table('invoice_nums')
						->select('invoice_num')
						->where('id','2')
						->where('name','Purchase')
						->get();
		$innum = $invoice[0]->invoice_num;

    	return view('purchase.purchaseadd', compact('innum'));
    }
    public function purchaseconfirm(Request $request){
    	$request->session()->put('billno',$request->input('billno'));
    	$request->session()->put('pdate',$request->input('pudate'));
    	$request->session()->put('purid',$request->input('purid'));
    	$request->session()->put('purname',$request->input('purname'));
    	$request->session()->put('phone',$request->input('phone'));
    	$request->session()->put('vehiclenump',$request->input('vehiclenump'));
    	$request->session()->put('vehicleidp',$request->input('vehicleidp'));
    	$request->session()->put('itemnamep',$request->input('itemnamep'));
    	$request->session()->put('itemidp',$request->input('prodidp'));
    	$request->session()->put('ratep',$request->input('itemratep'));
    	$request->session()->put('quantityp',$request->input('itemqtyp'));
    	$request->session()->put('totalp',$request->input('itemtotalp'));
    	$request->session()->put('amountpayed',$request->input('amountpayed'));
    	$request->session()->put('pmode',$request->input('psmode'));
    	$request->session()->put('pref',$request->input('psref'));
    	return view('purchase.purchaseconfirm');

    }

    public function purchaseaddmain(Request $request){

    	$purid = $request->purid;
    	if($request->purid == ''){
    		$purchaser = new Purchaser();
    		$purchaser->name = $request->purname;
    		$purchaser->phone = $request->phone;
    		$purchaser->amount = 0;
    		$purchaser->save();
    		$purid = $purchaser->id;
    	}


		$salesdet = new PurchaseBill();
		$salesdet->bill_no = $request->billno;
		$salesdet->pdate = $request->pudate;
		$salesdet->purid = $purid;
		$salesdet->vehicleid = $request->vehicleidp;
		$salesdet->vehiclenum = $request->vehiclenump;
		$salesdet->product_id = $request->prodidp;
		$salesdet->prodname = $request->itemnamep;
		$salesdet->rate = $request->itemratep;
		$salesdet->quantity = $request->itemqtyp;
		$salesdet->total = $request->itemtotalp;
		$salesdet->amountpayed = $request->amountpayed;
		$salesdet->amountpay = 0;
		$salesdet->save ();

		$salespayment = new PurchasePayment();
		$salespayment->billno = $salesdet->bill_no;
		$salespayment->desc = "Bill";
		$salespayment->billdate = $salesdet->pdate;
		$salespayment->amount = $request->amountpayed;
		$salespayment->mode = $request->pmode;
		$salespayment->ref = $request->pref;
		$salespayment->save ();

		$amnt = $request->itemtotalp - $request->amountpayed;

		DB::update("UPDATE invoice_nums SET invoice_num = invoice_num + 1 where id = 2");

		DB::update("UPDATE products SET cft = cft + '$request->itemqtyp' where id = '$request->prodidp'");

		$credit_amount = $request->itemtotalp - $request->amountpayed;

		DB::update("UPDATE purchasers SET amount = amount + '$amnt' where id = '$purid'");

    	return redirect('purchase');
    }
  
    public function purchasereport(){
    	return view('purchase.purchasereport');
    }

    public function purchasereportdata(Request $request){
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		$purchase_bill_data = DB::table('purchase_bills')
										->select('*')
										->whereBetween('pdate',[$datefrom,$dateto])
										->orderBy('id','DESC')
										->get();

		return view('purchase.purchasereportlist', compact('purchase_bill_data'));
	}


}