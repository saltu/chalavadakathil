<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Salestemp;
use App\Models\Product;
use App\Models\InvoiceNum;
use App\Models\Customer;
use App\Models\SalesBill;
use App\Models\SalesPayment;
use App\Models\OpenClose;
use App\Models\StockDetails;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SalesController extends Controller
{
	public function index()
	{
		if (!Auth::check()) {
			return redirect('/');
		}

		$date = date('Y-m-d');
		$status = 0;
		$amount = 0;
		$acc = DB::table('accounts')->select('*')->where('id', 1)->get();

		$amount = $acc[0]->amount;
		// foreach ($acc as $key => $value) {
		//     $amount = $value->amount;
		// }

		$acc1 = DB::table('open_closes')->select('*')->where('current_date', $date)->get();

		$status = $acc1[0]->id;

		// foreach ($acc1 as $key => $value) {
		//    	$status = $value->id;
		// }

		if ($status == 0) {
			$open = new OpenClose();
			$open->current_date = $date;
			$open->opening = $amount;
			$open->save();
		}

		$status1 = 0;
		$stock = DB::table('products')->select('*')->get();


		$stock1 = DB::table('stock_details')->select('*')->where('cudate', $date)->get();

		foreach ($stock1 as $key => $value) {
			$status1 = $value->id;
		}

		if ($status1 == 0) {
			foreach ($stock as $key => $value) {
				$stock = new StockDetails();
				$stock->material_id = $value->id;
				$stock->cft = $value->cft;
				$stock->cudate = $date;
				$stock->save();
			}
		}
		$invoice = DB::table('invoice_nums')
			->select('invoice_num')
			->where('id', '1')
			->where('name', 'Sales')
			->get();
		$innum = $invoice[0]->invoice_num;

		return view('sales.salesadd', compact('innum'));
	}


	public function salesconfirm(Request $request)
	{

		// echo $request->input('custid');die;
		if ($request->input('custid') == null) {
			return Redirect::back()->withInput(Input::all())->withErrors(['Please enter valid customer name']);
		} else {

			$request->session()->put('billno', $request->input('billno'));
			$request->session()->put('sdate', $request->input('cudate'));
			$request->session()->put('custid', $request->input('custid'));
			$request->session()->put('name', $request->input('custname'));
			$request->session()->put('phone', $request->input('phone'));
			$request->session()->put('vehiclenum', $request->input('vehiclenum'));
			$request->session()->put('vehicleid', $request->input('vehicleid'));
			$request->session()->put('itemname', $request->input('itemname'));
			$request->session()->put('itemid', $request->input('prodid'));
			$request->session()->put('rate', $request->input('itemrate'));
			$request->session()->put('quantity', $request->input('itemqty'));
			$request->session()->put('extrafare', $request->input('extrafare'));
			$request->session()->put('total', $request->input('itemtotal'));
			$request->session()->put('amountpayed', $request->input('amountpayed'));
			$request->session()->put('mode', $request->input('smode'));
			$request->session()->put('ref', $request->input('sref'));
			return view('sales.salesconfirm');
		}
	}

	public function salesaddmain(Request $request)
	{

		$custid = $request->custid;
		if ($request->custid == '') {
			if (Customer::where('name', $request->input('custname'))->count() > 0) {
				return Redirect::back()->withInput(Input::all())->withErrors(['Please enter valid customer name']);
			}
		}

		if (SalesBill::where('bill_no', $request->input('billno'))->count() > 0) {
			return Redirect::back()->withInput(Input::all())->withErrors(['Allready entered this bill']);
		}

		$salesdet = new SalesBill();
		$salesdet->bill_no = $request->billno;
		$salesdet->sdate = $request->cudate;
		$salesdet->custid = $custid;
		$salesdet->vehicleid = $request->vehicleid;
		$salesdet->vehiclenum = $request->vehiclenum;
		$salesdet->product_id = $request->prodid;
		$salesdet->prodname = $request->itemname;
		$salesdet->rate = $request->itemrate;
		$salesdet->quantity = $request->itemqty;
		$salesdet->extrafare = $request->extrafare;
		$salesdet->total = $request->itemtotal;
		$salesdet->amountpayed = $request->amountpayed;
		$salesdet->amountpay = 0;
		$salesdet->save();

		$salespayment = new SalesPayment();
		$salespayment->billno = $salesdet->bill_no;
		$salespayment->desc = "Bill";
		$salespayment->billdate = $salesdet->sdate;
		$salespayment->amount = $request->amountpayed;
		$salespayment->mode = $request->smode;
		$salespayment->ref = $request->sref;
		$salespayment->save();

		$amnt = $request->itemtotal - $request->amountpayed;

		DB::update("UPDATE invoice_nums SET invoice_num = invoice_num + 1 where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->amountpayed' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->amountpayed' where id = 2");


		DB::update("UPDATE products SET cft = cft - '$request->itemqty' where id = '$request->prodid'");

		$credit_amount = $request->itemtotal - $request->amountpayed;

		DB::update("UPDATE accounts SET amount = amount + '$credit_amount' where id = 5");

		DB::update("UPDATE customers SET amount = amount + '$amnt' where id = '$custid'");

		return redirect('sales');
	}

	public function item(Request $request)
	{

		$json = array();

		$status = Product::where('name', [$request->name])
			->get();

		if (count($status) > 0) {
			$json['count'] = '1';
		} else {
			$json['count'] = '0';
		}

		echo json_encode($json);
	}

	public function salesreport()
	{
		return view('sales.salesreport');
	}

	public function salesreportdata(Request $request)
	{
		// echo $request->datefrom.$request->dateto;die;
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		$sales_bill_data = DB::table('sales_bills')
			->select('*')
			->whereBetween('sdate', [$datefrom, $dateto])
			->orderBy('id', 'DESC')
			->get();

		return view('sales.salesreportlist', compact('sales_bill_data'));
	}

	// public function creditreport(){
	//    	return view('sales.credit');
	//    }

	public function daily_credit()
	{
		return view('sales.daily_credit');
	}
	public function daily_creditreportdata(Request $request)
	{
		$credit_data = DB::table('sales_bills')
			->select('*')
			->where('sdate', $request->creditdate)
			->orderBy('id', 'DESC')
			->get();

		return view('sales.daily_creditlist', compact('credit_data'));
	}

	public function creditlistreport()
	{

		$credit_customers = DB::table('customers')
			->select('*')
			->where('amount', '>', '0')
			->orderBy('id', 'DESC')
			->get();
		return view('sales.creditlist', compact('credit_customers'));
	}

	//    public function creditreportdata(Request $request){
	// 	$datefrom = $request->datefrom;
	// 	$dateto = $request->dateto;

	// 	$sales_bill_data = DB::table('sales_bills')
	// 									->select('*')
	// 									->whereBetween('sdate',[$datefrom,$dateto])
	// 									->orderBy('id','DESC')
	// 									->get();

	// 	return view('sales.salescreditlist', compact('sales_bill_data'));
	// }

	public function salesentry()
	{
		return view('sales.salesentry');
	}
}
