<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesBill;
use App\Models\SalesPayment;
use App\Models\InvoiceNum;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AccountController extends Controller
{
    public function index(){
    	return view('account.index');
    }

    public function billview(){
    	return view('account.billview');
    }

    public function billdetails(Request $request){
    	$date = date('Y-m-d');
    	
    	$sales_bill_data = DB::table('sales_bills')->select('bill_no')->groupBy('bill_no')->where('sdate',$date)->get();
    	// echo $sales_bill_data;
    	if(!$sales_bill_data->isEmpty()){
	    	foreach ($sales_bill_data as $value) {
	    		// echo $value->bill_no;
	    		$bill_no = $value->bill_no;
	    		$bill_count = SalesBill::where('bill_no',$bill_no)->count();
	    		// echo ",".$bill_count."<br>";
	    		if($bill_count > 1){
	    			$request->session()->put('bill_no', $bill_no);
	    			return view('account.billlist', compact('bill_no'));
		    	} else {
		    		echo "No Repeated bills found";
		    	}
	    	}
	    } else {
	    	echo "No Repeated bills found";
	    }
    }

    public function billdelete(Request $request){
    	// print_r($request->bill_no);die;
    	$idForDelete = DB::table("sales_bills")->where("bill_no", $request->bill_no)->first();
    	$id1 = $idForDelete->id;
    	$idForDeletepay = DB::table("sales_payments")->where("billno", $request->bill_no)->first();
    	$id2 =  $idForDeletepay->id;
    	DB::table("sales_bills")->where("id",$id1)->delete();
    	DB::table("sales_payments")->where("id",$id2)->delete();
    	$request->session()->forget('bill_no');
    	return Redirect('account');
    }

    public function billupdate(Request $request){

		$bill_number = $request->bill_number_up;

		InvoiceNum::where('id','1')->update(array('invoice_num' => $bill_number));

		return Redirect('account/bill');
    }
    
}
