<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanDetails extends Model
{
    protected $fillable = [
        'lddate', 'source_id', 'amount', 'type'
    ];

    protected $hidden = [];
}
