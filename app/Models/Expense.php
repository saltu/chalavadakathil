<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
   protected $fillable = [
        'description', 'amount', 'ex_date', 'type'
    ];

    protected $hidden = [];
}
