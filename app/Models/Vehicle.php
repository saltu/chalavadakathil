<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
   protected $fillable = [
        'number', 'type', 'capacity', 'descr', 'km', 'permit', 'tax', 'test', 'insurance', 'ownername'
    ];

    protected $hidden = [];
}