<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseBill extends Model
{
   protected $fillable = [
       'bill_no', 'pdate', 'custid','vehicleid','product_id', 'rate', 'quantity', 'total'
    ];

    protected $hidden = [];
}
