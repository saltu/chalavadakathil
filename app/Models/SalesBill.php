<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesBill extends Model
{
    protected $fillable = [
        'bill_no', 'sdate', 'custid', 'name', 'phone','vehicleid', 'vehiclenum','product_id', 'prodname', 'rate', 'quantity','extrafare', 'total'
    ];

    protected $hidden = [];
}
