<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenClose extends Model
{
    protected $fillable = [
        'current_date', 'opening', 'closing'
    ];

    protected $hidden = [];
}
