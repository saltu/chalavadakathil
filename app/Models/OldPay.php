<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OldPay extends Model
{
   protected $fillable = [
        'oname', 'oitem', 'oamount', 'odate'
    ];

    protected $hidden = [];
}
