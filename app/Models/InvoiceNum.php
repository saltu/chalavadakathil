<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceNum extends Model
{
     protected $fillable = [
        'name', 'invoice_num'
    ];

    protected $hidden = [];
}
