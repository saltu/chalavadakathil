<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockDetails extends Model
{
    protected $fillable = [
       'material_id', 'cft', 'cudate'
    ];

    protected $hidden = [];
}
