<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   protected $fillable = [
        'vehicle_id', 'epermit', 'etax', 'etest', 'einsurance', 'view'
    ];

    protected $hidden = [];
}
