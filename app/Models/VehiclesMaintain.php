<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehiclesMaintain extends Model
{
    protected $fillable = [
        'mdate', 'vehicle_id', 'descrp', 'amount'
    ];

    protected $hidden = [];
}
