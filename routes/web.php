<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {})->name('/');

// Main Page
Route::get('/', 'Controller@index')->name('/');

Auth::routes();

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

//login
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post ('/login', 'Auth\LoginController@login')->name('login');
Route::get ( '/logout', 'Auth\LoginController@logout' )->name('logout');
Route::get('/register', function () {
    return redirect('/');
});


//Home Page
Route::get('/home', 'HomeController@index')->name('home');

//Backup
Route::get('backup','HomeController@backup')->name('backup');

//Customer
Route::prefix('customer')->group(function(){
    Route::get('/', 'CustomerController@index')->name('customer');
    Route::post('/add', 'CustomerController@add')->name('customer.add');
    Route::get('/edit/{id}', 'CustomerController@edit')->name('customer.edit');
    Route::get('/delete/{id}', 'CustomerController@delete')->name('customer.delete');
});

//Purchaser
Route::prefix('purchaser')->group(function(){
    Route::get('/', 'PurchaserController@index')->name('purchaser');
    Route::post('/add', 'PurchaserController@add')->name('purchaser.add');
    Route::get('/edit/{id}', 'PurchaserController@edit')->name('purchaser.edit');
    Route::get('/delete/{id}', 'PurchaserController@delete')->name('purchaser.delete');
});

//Stocks
Route::prefix('stock')->group(function(){
    Route::get('/', 'StocksController@index')->name('stock');    
    Route::post('/add', 'StocksController@add')->name('stock.add');
    Route::get('/edit/{id}', 'StocksController@edit')->name('stock.edit');
    Route::get('/delete/{id}', 'StocksController@delete')->name('stock.delete');
});

//Vehicle
Route::prefix('vehicle')->group(function(){
    Route::get('/', 'VehicleController@index')->name('vehicle');    
    Route::post('/add', 'VehicleController@add')->name('vehicle.add');
    Route::get('/edit/{id}', 'VehicleController@edit')->name('vehicle.edit');
    Route::get('/delete/{id}', 'VehicleController@delete')->name('vehicle.delete');
    // report
    Route::get('/dates', 'VehicleController@dates')->name('vehicle.dates');
    // maintiances
    Route::get('/maintain', 'VehicleController@maintain')->name('vehicle.maintain');
    Route::post('/maintain/add', 'VehicleController@madd')->name('vehicle.maintain.add');
    Route::get('/maintain/edit/{id}', 'VehicleController@medit')->name('vehicle.maintain.edit');
    Route::get('/maintain/delete/{id}', 'VehicleController@mdelete')->name('vehicle.maintain.delete');

    //autocomplete Number
    Route::get('number', 'VehicleController@vehiclenum')->name('vehicle.number');
    Route::get('number/details', 'VehicleController@vehicledetails')->name('vehicle.number.details');

    Route::get('notify_view', 'VehicleController@ntf_view')->name('vehicle.notify_view');

});


//Sales
Route::prefix('sales')->group(function(){
    Route::get('/', 'SalesController@index')->name('sales');
    // Bill Save
    Route::post('add', 'SalesController@salesaddmain')->name('sales.add');
    
    // confirm
    Route::post('confirm', 'SalesController@salesconfirm')->name('sales.confirm');

    //Report
    Route::get('report', 'SalesController@salesreport')->name('sales.report');
    Route::post('salesreport/data', 'SalesController@salesreportdata')->name('sales.salesreport.data');

    Route::get('credit', 'SalesController@creditreport')->name('sales.credit');
    Route::post('creditreport/data', 'SalesController@creditreportdata')->name('sales.creditreport.data');

    Route::get('creditlist', 'SalesController@creditlistreport')->name('sales.creditlist');
    Route::get('daily_credit', 'SalesController@daily_credit')->name('sales.daily_credit');
    Route::post('daily_creditreport/details', 'SalesController@daily_creditreportdata')->name('sales.daily_creditreport.details');

    //autocomplete customer
    Route::get('customer/custname', 'CustomerController@custname')->name('sales.customer.custname');
    Route::get('customer/details', 'CustomerController@custdetails')->name('sales.customer.details');
    Route::get('customer/name', 'CustomerController@name')->name('sales.customer.name');
    Route::get('customer/item', 'SalesController@item')->name('sales.customer.item');

    //autocomplete product
    Route::get('product/prodname', 'StocksController@prodname')->name('sales.product.prodname');
    Route::get('product/details', 'StocksController@proddetails')->name('sales.product.details');

    //autocomplete Number
    Route::get('number', 'VehicleController@vehiclenumm')->name('sales.vehicle.number');
    Route::get('number/details', 'VehicleController@vehicledetailss')->name('sales.vehicle.number.details');
});

//Purchase
Route::prefix('purchase')->group(function(){
    Route::get('/', 'PurchaseController@index')->name('purchase');    
    // Bill Save
    Route::post('add', 'PurchaseController@purchaseaddmain')->name('purchase.add');

    // Confirm  
     Route::post('confirm', 'PurchaseController@purchaseconfirm')->name('purchase.confirm');

    //Report
    Route::get('report', 'PurchaseController@purchasereport')->name('purchase.report');
    Route::post('purchasereport/data', 'PurchaseController@purchasereportdata')->name('purchase.purchasereport.data');

    //autocomplete purchaser
    Route::get('purchaser/purname', 'PurchaserController@purname')->name('purchase.purchaser.purname');
    Route::get('purchaser/details', 'PurchaserController@purdetails')->name('purchase.purchaser.details');

    //autocomplete product
    Route::get('product/prodname', 'StocksController@prodname')->name('sales.product.prodname');
    Route::get('product/details', 'StocksController@proddetails')->name('sales.product.details');

    //autocomplete Number
    Route::get('number', 'VehicleController@vehiclenumm')->name('purchase.vehicle.number');
    Route::get('number/details', 'VehicleController@vehicledetailss')->name('purchase.vehicle.number.details');

});

//Payment
Route::prefix('payment')->group(function(){
    //Sales
    Route::get('sales', 'SalesPaymentController@index')->name('payment.sales');
    Route::get('psbill/no', 'SalesPaymentController@billno')->name('payment.sales.billno');
    Route::get('psbill/details', 'SalesPaymentController@billdetails')->name('payment.sales.bill.details');
    Route::post('psbill/add', 'SalesPaymentController@billadd')->name('payment.sales.bill.add');
    
    //Purchase
    Route::get('purchase', 'PurchasePaymentController@index')->name('payment.purchase');
    Route::get('ppbill/no', 'PurchasePaymentController@billno')->name('payment.purchase.billno');
    Route::get('ppbill/details', 'PurchasePaymentController@billdetails')->name('payment.purchase.bill.details');
    Route::post('ppbill/add', 'PurchasePaymentController@billadd')->name('payment.purchase.bill.add');
    
    //Customer
    Route::get('customer', 'SalesPaymentController@customer')->name('payment.customer');

    //autocomplete customer
    Route::get('customer/custname', 'CustomerController@custname')->name('sales.customer.custname');
    Route::get('customer/details', 'CustomerController@custdetails')->name('sales.customer.details');
    // biil
    Route::post('customer/add', 'SalesPaymentController@custbilladd')->name('payment.customer.bill.add');
    // report
    Route::post('customer/report/list', 'SalesPaymentController@customerreportdetails')->name('payment.customer.report.list');
    

    //Purchaser
    Route::get('purchaser', 'PurchasePaymentController@purchaser')->name('payment.purchaser');
    //autocomplete purchaser
    Route::get('purchaser/purname', 'PurchaserController@purname')->name('purchase.purchaser.purname');
    Route::get('purchaser/details', 'PurchaserController@purdetails')->name('purchase.purchaser.details');
    // bill
    Route::post('purchaser/add', 'PurchasePaymentController@purbilladd')->name('payment.purchaser.bill.add');
    // report
    Route::post('purchaser/report/list', 'PurchasePaymentController@purchaserreportdetails')->name('payment.purchaser.report.list');


    //OLD
    Route::get('old', 'SalesPaymentController@old')->name('payment.old');
    Route::post('old/add', 'SalesPaymentController@oldadd')->name('payment.old.add');

    
});

//Expense
Route::prefix('expense')->group(function(){
    Route::get('/', 'ExpenseController@index')->name('expense');
    Route::post('/add', 'ExpenseController@add')->name('expense.add');
    Route::get('/edit/{id}', 'ExpenseController@edit')->name('expense.edit');
    Route::get('/delete/{id}', 'ExpenseController@delete')->name('expense.delete');
});

//DailyReport
Route::prefix('dailyreport')->group(function(){
    //Daily
    Route::get('report', 'DailyReportController@index')->name('dailyreport.daily');
    Route::post('report/details', 'DailyReportController@reportdetails')->name('dailyreport.daily.details');
    //custom report
    Route::get('customreport', 'DailyReportController@creport')->name('dailyreport.creport');
    Route::post('customreport/data', 'DailyReportController@creportdetails')->name('dailyreport.custom.data');

});


//Loan
Route::prefix('loan')->group(function(){
    Route::get('/', 'LoanController@index')->name('loan');
    Route::post('/add', 'LoanController@add')->name('loan.add');
    Route::get('/edit/{id}', 'LoanController@edit')->name('loan.edit');
    Route::get('report', 'LoanController@report')->name('loan.report');
    Route::get('sourcename', 'LoanController@sourcename')->name('loan.source.name');
    Route::post('report/data', 'LoanController@reportdata')->name('loan.report.data');
});


//Loan
Route::prefix('rental')->group(function(){
    Route::get('/', 'RentalController@index')->name('rental');
    Route::post('/add', 'RentalController@add')->name('rental.add');
});


//Management
Route::prefix('account')->group(function(){
    Route::get('/', 'AccountController@index')->name('account');    
    Route::post('/bill/details', 'AccountController@billdetails')->name('account.bill.details');
    Route::post('/bill/delete', 'AccountController@billdelete')->name('account.bill.delete');
    Route::get('/bill', 'AccountController@billview')->name('account.bill');
    Route::post('/bill/update', 'AccountController@billupdate')->name('account.bill.update');
});


