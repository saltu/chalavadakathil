-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2020 at 10:00 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chalavadakathil`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'Account', 175101, NULL, NULL),
(2, 'Sales', 136000, NULL, NULL),
(3, 'Purchase', 112000, NULL, NULL),
(4, 'Expense', 26010, NULL, NULL),
(5, 'Sales Credit', 58600, NULL, NULL),
(6, 'Purchase Credit', 59000, NULL, NULL),
(7, 'Loan Credit', 10000, NULL, NULL),
(8, 'Loan Debit', 7000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'Tester1', '1234567890', 12500, '2019-07-12 06:07:32', '2019-07-12 06:07:32'),
(2, 'Tester2', '9876543210', 0, '2019-07-12 06:07:41', '2019-07-12 06:07:41'),
(3, 'Prithviraj.K.R', '7012525741', 1000, '2019-07-23 03:00:08', '2019-07-23 03:00:08'),
(4, 'Prithviraj.K.R', '7012525741', 1000, '2019-07-24 00:45:02', '2019-07-24 00:45:02'),
(5, 'Prithviraj.K.R', '7012525741', 1000, '2019-07-24 01:10:25', '2019-07-24 01:10:25'),
(7, 'Prithviraj.K.R', '7012525741', 17100, '2019-07-26 01:59:29', '2019-07-26 01:59:29'),
(8, 'Ram', '7252874130', 1000, '2019-07-26 02:01:43', '2019-07-26 02:01:43'),
(9, 'customer5', '0', 10000, '2019-08-02 23:33:27', '2019-08-02 23:33:27'),
(10, 'customer7', '0', 8000, '2019-08-02 23:35:58', '2019-08-02 23:35:58');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `description`, `amount`, `ex_date`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Vehicle maintains', 3000, '2019-07-17', 'Dr', '2019-07-17 00:06:23', '2019-07-17 00:06:23'),
(2, 'Tea', 10, '2019-07-17', 'Dr', '2019-07-17 00:17:11', '2019-07-17 00:17:11'),
(3, 'success celebration', 2000, '2019-07-24', 'Dr', '2019-07-24 00:54:11', '2019-07-24 00:54:11'),
(4, 'Vehicle maintains', 3000, '2019-08-03', 'Dr', '2019-08-02 23:05:45', '2019-08-02 23:05:45'),
(5, 'petrol', 1000, '2019-08-08', 'Cr', '2019-08-08 13:56:35', '2019-08-08 13:56:35'),
(6, 'Vehicle maintains', 2000, '2019-08-09', NULL, '2019-08-09 05:37:42', '2019-08-09 05:37:42'),
(7, 'tea', 1000, '2019-08-09', 'Dr', '2019-08-09 06:12:29', '2019-08-09 06:12:29'),
(8, 'petrol', 1000, '2019-08-09', 'Cr', '2019-08-09 06:13:42', '2019-08-09 06:13:42');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_nums`
--

CREATE TABLE `invoice_nums` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_nums`
--

INSERT INTO `invoice_nums` (`id`, `name`, `invoice_num`, `created_at`, `updated_at`) VALUES
(1, 'Sales', 42, NULL, NULL),
(2, 'Purchase', 27, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(10) UNSIGNED NOT NULL,
  `ldate` date DEFAULT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lamount` double(8,2) NOT NULL,
  `pamount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `ldate`, `source`, `lamount`, `pamount`, `created_at`, `updated_at`) VALUES
(1, '2019-07-17', 'loan1', 100000.00, 10000.00, '2019-07-17 00:14:44', '2019-07-17 00:14:44'),
(2, '2019-07-23', 'loan2', 5000.00, 3000.00, '2019-07-23 05:20:54', '2019-07-23 05:20:54'),
(3, '2019-08-03', 'loan3', 50000.00, 0.00, '2019-08-03 00:07:27', '2019-08-03 00:07:27'),
(4, '2019-08-09', 'loan2', 10000.00, 0.00, '2019-08-09 06:11:09', '2019-08-09 06:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `loan_details`
--

CREATE TABLE `loan_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `lddate` date DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loan_details`
--

INSERT INTO `loan_details` (`id`, `lddate`, `source_id`, `amount`, `type`, `created_at`, `updated_at`) VALUES
(1, '2019-07-17', 1, 100000.00, 'Cr', '2019-07-17 00:14:44', '2019-07-17 00:14:44'),
(2, '2019-07-17', 1, 3000.00, 'Dr', '2019-07-17 00:16:45', '2019-07-17 00:16:45'),
(3, '2019-07-23', 2, 5000.00, 'Cr', '2019-07-23 05:20:54', '2019-07-23 05:20:54'),
(4, '2019-07-23', 2, 3000.00, 'Dr', '2019-07-23 05:43:14', '2019-07-23 05:43:14'),
(5, '2019-08-03', 3, 50000.00, 'Cr', '2019-08-03 00:07:27', '2019-08-03 00:07:27'),
(6, '2019-08-03', 1, 7000.00, 'Dr', '2019-08-03 00:07:38', '2019-08-03 00:07:38'),
(7, '2019-08-09', 4, 10000.00, 'Cr', '2019-08-09 06:11:09', '2019-08-09 06:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2019_04_18_085048_create_purchase_bill_details_table', 11),
(18, '2019_03_04_070830_create_sales_bills_table', 10),
(15, '2019_03_04_071041_create_open_closes_table', 9),
(14, '2019_03_11_040118_create_purchasers_table', 8),
(13, '2019_03_11_040057_create_purchase_bill_temps_table', 8),
(20, '2019_03_11_040048_create_purchase_bills_table', 11),
(11, '2019_03_11_040031_create_purchase_payments_table', 8),
(10, '2019_03_04_070954_create_accounts_table', 7),
(9, '2019_03_09_053954_create_sales_payments_table', 6),
(8, '2019_03_08_053806_create_sales_bill_temps_table', 5),
(7, '2019_03_08_053700_create_customers_table', 5),
(19, '2019_04_18_072235_create_sales_bill_details_table', 10),
(5, '2019_03_04_102935_create_invoice_nums_table', 4),
(4, '2019_03_04_070900_create_expenses_table', 3),
(3, '2019_03_04_053630_create_products_table', 2),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(1, '2014_10_12_000000_create_users_table', 1),
(22, '2019_04_23_054853_create_sales_estimates_table', 12),
(24, '2019_07_10_063915_create_vehicles_table', 13),
(25, '2019_07_10_124257_create_vehicles_maintains_table', 14),
(26, '2019_07_16_114641_create_loans_table', 15),
(27, '2019_07_16_115110_create_loan_details_table', 15),
(28, '2019_07_16_172109_create_stock_details_table', 16),
(29, '2019_07_25_085302_create_salestemp', 17),
(30, '2019_07_25_161823_create_sales_temps_table', 18),
(31, '2019_07_25_163137_create_salestemps_table', 19),
(32, '2019_07_29_071401_create_notifictions_table', 20),
(33, '2019_08_01_054617_create_notifications_table', 21),
(34, '2019_08_03_062930_create_old_pays_table', 22),
(35, '2019_08_03_065518_create_rentals_table', 23);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `epermit` date DEFAULT NULL,
  `etax` date DEFAULT NULL,
  `etest` date DEFAULT NULL,
  `einsurance` date DEFAULT NULL,
  `view` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `vehicle_id`, `epermit`, `etax`, `etest`, `einsurance`, `view`, `created_at`, `updated_at`) VALUES
(9, 'KL-88-2012', '1969-12-01', '1969-12-01', '1969-12-01', '1969-12-01', 1, '2019-08-02 23:04:10', '2019-08-02 23:04:10'),
(10, 'KL-36-2586', '1969-12-01', '1969-12-01', '1969-12-01', '1969-12-01', 1, '2019-08-02 23:05:08', '2019-08-02 23:05:08'),
(11, 'kl-02-2012', '2019-07-09', '2019-07-09', '2019-07-09', '2019-07-09', 1, '2019-08-09 05:36:05', '2019-08-09 05:36:05'),
(12, 'kl-10-1000', '1969-12-01', '1969-12-01', '1969-12-01', '1969-12-01', 1, '2019-08-09 05:37:00', '2019-08-09 05:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `old_pays`
--

CREATE TABLE `old_pays` (
  `id` int(10) UNSIGNED NOT NULL,
  `oname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oamount` double(8,2) DEFAULT NULL,
  `odate` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `old_pays`
--

INSERT INTO `old_pays` (`id`, `oname`, `oitem`, `oamount`, `odate`, `created_at`, `updated_at`) VALUES
(1, 'old1', 'mm', 1000.00, '2019-08-03', '2019-08-03 01:11:57', '2019-08-03 01:11:57'),
(2, 'old2', 'aaa', 1000.00, '2019-08-09', '2019-08-09 06:37:18', '2019-08-09 06:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `open_closes`
--

CREATE TABLE `open_closes` (
  `id` int(10) UNSIGNED NOT NULL,
  `current_date` date DEFAULT NULL,
  `opening` double DEFAULT NULL,
  `closing` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `open_closes`
--

INSERT INTO `open_closes` (`id`, `current_date`, `opening`, `closing`, `created_at`, `updated_at`) VALUES
(1, '2019-07-12', 0, NULL, '2019-07-12 06:07:15', '2019-07-12 06:07:15'),
(2, '2019-07-15', 82480, NULL, '2019-07-14 22:45:59', '2019-07-14 22:45:59'),
(3, '2019-07-16', -158520, NULL, '2019-07-15 23:28:36', '2019-07-15 23:28:36'),
(4, '2019-07-17', 0, NULL, '2019-07-16 23:55:58', '2019-07-16 23:55:58'),
(5, '2019-07-18', 69990, NULL, '2019-07-18 06:19:39', '2019-07-18 06:19:39'),
(6, '2019-07-19', 73990, NULL, '2019-07-18 23:27:50', '2019-07-18 23:27:50'),
(7, '2019-07-22', 73990, NULL, '2019-07-22 00:07:22', '2019-07-22 00:07:22'),
(8, '2019-07-23', 73990, NULL, '2019-07-23 00:33:30', '2019-07-23 00:33:30'),
(9, '2019-07-24', 76990, NULL, '2019-07-23 23:18:25', '2019-07-23 23:18:25'),
(10, '2019-07-25', 74990, NULL, '2019-07-24 21:30:44', '2019-07-24 21:30:44'),
(11, '2019-07-26', 74990, NULL, '2019-07-25 22:48:39', '2019-07-25 22:48:39'),
(12, '2019-07-27', 81990, NULL, '2019-07-27 01:30:50', '2019-07-27 01:30:50'),
(13, '2019-07-28', 83990, NULL, '2019-07-27 21:57:48', '2019-07-27 21:57:48'),
(14, '2019-07-29', 83990, NULL, '2019-07-29 00:27:40', '2019-07-29 00:27:40'),
(15, '2019-07-30', 83990, NULL, '2019-07-30 12:37:59', '2019-07-30 12:37:59'),
(16, '2019-07-31', 83990, NULL, '2019-07-30 23:59:43', '2019-07-30 23:59:43'),
(17, '2019-08-01', 83990, NULL, '2019-08-01 01:21:28', '2019-08-01 01:21:28'),
(18, '2019-08-02', 83990, NULL, '2019-08-01 21:58:07', '2019-08-01 21:58:07'),
(19, '2019-08-03', 83990, NULL, '2019-08-02 23:03:22', '2019-08-02 23:03:22'),
(20, '2019-08-05', 141490, NULL, '2019-08-05 00:11:29', '2019-08-05 00:11:29'),
(21, '2019-08-07', 155990, NULL, '2019-08-06 23:00:01', '2019-08-06 23:00:01'),
(22, '2019-08-08', 165990, NULL, '2019-08-07 18:34:06', '2019-08-07 18:34:06'),
(23, '2019-08-09', 164990, NULL, '2019-08-09 05:35:11', '2019-08-09 05:35:11'),
(24, '2019-10-30', 173101, NULL, '2019-10-30 10:53:12', '2019-10-30 10:53:12'),
(25, '2019-11-09', 173101, NULL, '2019-11-09 15:00:50', '2019-11-09 15:00:50'),
(26, '2019-12-13', 173101, NULL, '2019-12-13 05:44:42', '2019-12-13 05:44:42'),
(27, '2020-02-27', 173101, NULL, '2020-02-27 08:58:35', '2020-02-27 08:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cft` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `cft`, `created_at`, `updated_at`) VALUES
(1, 'material1', -100, '2019-07-17 00:09:53', '2019-07-17 00:09:53'),
(2, 'material2', 5000, '2019-07-17 00:10:01', '2019-07-17 00:10:01'),
(3, 'material3', -1500, '2019-07-17 00:10:06', '2019-07-17 00:10:06'),
(4, 'material4', -1000, '2019-07-17 00:10:12', '2019-07-17 00:10:12'),
(5, 'material5', 2000, '2019-07-17 00:10:16', '2019-07-17 00:10:16'),
(6, 'material6', 2000, '2019-08-02 23:06:09', '2019-08-02 23:06:09'),
(7, 'material7', 4000, '2019-08-09 05:38:08', '2019-08-09 05:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `purchasers`
--

CREATE TABLE `purchasers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchasers`
--

INSERT INTO `purchasers` (`id`, `name`, `phone`, `amount`, `gst`, `created_at`, `updated_at`) VALUES
(1, 'Tester1', '1234567890', 31000.00, NULL, '2019-07-12 06:07:52', '2019-07-12 06:07:52'),
(2, 'Tester2', '9876543210', 0.00, NULL, '2019-07-12 06:08:03', '2019-07-12 06:08:03'),
(3, 'purchaser7', '9446858804', 3000.00, NULL, '2019-07-24 00:41:51', '2019-07-24 00:41:51'),
(4, 'purchaser9', '0', 5000.00, NULL, '2019-08-02 23:45:58', '2019-08-02 23:45:58');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bills`
--

CREATE TABLE `purchase_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdate` date DEFAULT NULL,
  `purid` int(11) DEFAULT NULL,
  `vehicleid` int(11) DEFAULT NULL,
  `vehiclenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `prodname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` double(8,2) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpayed` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_bills`
--

INSERT INTO `purchase_bills` (`id`, `bill_no`, `pdate`, `purid`, `vehicleid`, `vehiclenum`, `product_id`, `prodname`, `rate`, `quantity`, `total`, `amountpayed`, `amountpay`, `created_at`, `updated_at`) VALUES
(1, '20', '2019-07-17', 1, 1, NULL, 1, NULL, 100.00, 1000, 100000, 50000, 20000, '2019-07-17 00:10:59', '2019-07-17 00:10:59'),
(2, '21', '2019-07-24', 3, 3, NULL, 3, NULL, 5.00, 3000, 15000, 10000, 1000, '2019-07-24 00:41:51', '2019-07-24 00:41:51'),
(3, '22', '2019-07-24', 3, 3, NULL, 4, NULL, 2.00, 3000, 6000, 6000, 0, '2019-07-24 02:54:30', '2019-07-24 02:54:30'),
(4, '23', '2019-07-26', 3, 3, NULL, 3, NULL, 1.00, 3000, 3000, 3000, 0, '2019-07-26 03:43:53', '2019-07-26 03:43:53'),
(5, '24', '2019-07-26', 1, 5, NULL, 5, NULL, 1.00, 2000, 2000, 1000, 0, '2019-07-26 03:45:51', '2019-07-26 03:45:51'),
(6, '25', '2019-08-03', 4, NULL, 'KL-22-6969', NULL, 'material10', 10.00, 1000, 10000, 5000, 0, '2019-08-02 23:45:58', '2019-08-02 23:45:58'),
(7, '26', '2019-08-09', 1, 2, 'KL-29S-1993', 7, 'material7', 5.00, 3000, 15000, 5000, 0, '2019-08-09 06:10:20', '2019-08-09 06:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_payments`
--

CREATE TABLE `purchase_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_payments`
--

INSERT INTO `purchase_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(1, 20, 'Bill', '2019-07-17', 'pcash', '0', 50000.00, '2019-07-17 00:10:59', '2019-07-17 00:10:59'),
(2, 20, 'Payment', '2019-07-17', 'on', '0', 10000.00, '2019-07-17 00:17:38', '2019-07-17 00:17:38'),
(3, 1, 'Purchaser Payment', '2019-07-17', 'on', '0', 10000.00, '2019-07-17 00:20:21', '2019-07-17 00:20:21'),
(4, 21, 'Bill', '2019-07-24', 'pcash', '0', 10000.00, '2019-07-24 00:41:51', '2019-07-24 00:41:51'),
(5, 22, 'Bill', '2019-07-24', 'pcash', '0', 6000.00, '2019-07-24 02:54:30', '2019-07-24 02:54:30'),
(6, 23, 'Bill', '2019-07-26', 'pcash', '0', 3000.00, '2019-07-26 03:43:53', '2019-07-26 03:43:53'),
(7, 24, 'Bill', '2019-07-26', 'pcheque', 'xdffdx', 1000.00, '2019-07-26 03:45:51', '2019-07-26 03:45:51'),
(8, 25, 'Bill', '2019-08-03', 'pcash', '0', 5000.00, '2019-08-02 23:45:58', '2019-08-02 23:45:58'),
(9, 20, 'Payment', '2019-08-03', 'on', '0', 10000.00, '2019-08-02 23:58:23', '2019-08-02 23:58:23'),
(10, 3, 'Purchaser Payment', '2019-08-03', 'Cash', '0', 1000.00, '2019-08-03 00:01:07', '2019-08-03 00:01:07'),
(11, 26, 'Bill', '2019-08-09', 'pcash', '0', 5000.00, '2019-08-09 06:10:20', '2019-08-09 06:10:20'),
(12, 21, 'Payment', '2019-08-09', 'on', '0', 1000.00, '2019-08-09 06:36:10', '2019-08-09 06:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

CREATE TABLE `rentals` (
  `id` int(10) UNSIGNED NOT NULL,
  `rdesc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ramount` double(8,2) DEFAULT NULL,
  `rdate` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rentals`
--

INSERT INTO `rentals` (`id`, `rdesc`, `ramount`, `rdate`, `created_at`, `updated_at`) VALUES
(1, 'jcb', 10000.00, '2019-08-03', '2019-08-03 01:36:28', '2019-08-03 01:36:28'),
(2, 'aa', 1111.00, '2019-08-09', '2019-08-09 06:37:29', '2019-08-09 06:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bills`
--

CREATE TABLE `sales_bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sdate` date DEFAULT NULL,
  `custid` int(11) DEFAULT NULL,
  `vehicleid` int(11) DEFAULT NULL,
  `vehiclenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `prodname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` double(8,2) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `extrafare` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpayed` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bills`
--

INSERT INTO `sales_bills` (`id`, `bill_no`, `sdate`, `custid`, `vehicleid`, `vehiclenum`, `product_id`, `prodname`, `rate`, `quantity`, `extrafare`, `total`, `amountpayed`, `amountpay`, `created_at`, `updated_at`) VALUES
(1, '22', '2019-07-17', 1, 2, NULL, 1, NULL, 100.00, 500, NULL, 50000, 40000, 7000, '2019-07-17 00:13:28', '2019-07-17 00:13:28'),
(2, '23', '2019-07-23', 3, 2, NULL, 3, NULL, 1.00, 2000, NULL, 2000, 1000, 0, '2019-07-23 03:00:08', '2019-07-23 03:00:08'),
(3, '24', '2019-07-24', 4, 3, NULL, 2, NULL, 2.00, 3000, NULL, 6000, 5000, 0, '2019-07-24 00:45:02', '2019-07-24 00:45:02'),
(4, '25', '2019-07-24', 5, 3, NULL, 2, NULL, 2.00, 3000, NULL, 6000, 5000, 0, '2019-07-24 01:10:25', '2019-07-24 01:10:25'),
(5, '26', '2019-07-24', 2, 4, NULL, 2, NULL, 2.00, 3000, NULL, 6000, 6000, 0, '2019-07-24 02:42:23', '2019-07-24 02:42:23'),
(6, '27', '2019-07-26', 7, 4, NULL, 2, NULL, 1.00, 3000, NULL, 3000, 3000, 0, '2019-07-26 01:59:29', '2019-07-26 01:59:29'),
(7, '28', '2019-07-26', 8, 4, NULL, 2, NULL, 1.00, 3000, NULL, 3000, 3000, 0, '2019-07-26 02:01:43', '2019-07-26 02:01:43'),
(8, '29', '2019-07-26', 8, 3, NULL, 4, NULL, 1.00, 3000, NULL, 3000, 3000, 0, '2019-07-26 02:18:26', '2019-07-26 02:18:26'),
(9, '30', '2019-07-26', 8, 6, NULL, 3, NULL, 1.00, 3000, NULL, 3000, 2000, 0, '2019-07-26 02:21:42', '2019-07-26 02:21:42'),
(10, '31', '2019-07-27', 8, 4, NULL, 3, NULL, 1.00, 2500, NULL, 2500, 2000, 500, '2019-07-27 08:37:59', '2019-07-27 08:37:59'),
(11, '32', '2019-08-03', 9, NULL, 'KL-33-5858', 1, 'material7', 10.00, 500, NULL, 5000, 1000, 0, '2019-08-02 23:33:27', '2019-08-02 23:33:27'),
(12, '33', '2019-08-03', 10, NULL, 'KL-88-3633', NULL, 'material7', 10.00, 1000, NULL, 10000, 2000, 0, '2019-08-02 23:35:58', '2019-08-02 23:35:58'),
(13, '34', '2019-08-03', 9, 2, 'KL-29S-1993', 2, 'material2', 10.00, 2000, NULL, 20000, 10000, 0, '2019-08-02 23:38:09', '2019-08-02 23:38:09'),
(14, '35', '2019-08-03', 1, 3, 'KL-29M-123', 4, 'material4', 10.00, 1000, NULL, 10000, 5000, 0, '2019-08-02 23:39:01', '2019-08-02 23:39:01'),
(15, '36', '2019-08-05', 1, 1, 'KL-29A-01', 1, 'material1', 100.00, 100, NULL, 10000, 5000, 3500, '2019-08-05 00:29:08', '2019-08-05 00:29:08'),
(16, '37', '2019-08-05', 7, 1, 'KL-29A-01', 2, 'material2', 10.00, 1000, NULL, 10000, 5000, 0, '2019-08-05 05:16:13', '2019-08-05 05:16:13'),
(17, '38', '2019-08-07', 7, 2, 'KL-29S-1993', 2, 'material2', 11.00, 2000, NULL, 22000, 10000, 0, '2019-08-06 23:20:41', '2019-08-06 23:20:41'),
(18, '39', '2019-08-09', 1, 9, 'KL-88-2012', 7, 'material7', 10.00, 1000, NULL, 10000, 5000, 0, '2019-08-09 05:38:50', '2019-08-09 05:38:50'),
(19, '40', '2020-02-27', 7, 2, 'KL-29S-1993', 2, 'material2', 1.00, 1000, 50, 1050, 1000, 0, '2020-02-27 08:59:11', '2020-02-27 08:59:11'),
(20, '40', '2020-02-27', 7, 2, 'KL-29S-1993', 2, 'material2', 1.00, 1000, 50, 1050, 1000, 0, '2020-02-27 08:59:12', '2020-02-27 08:59:12');

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE `sales_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(1, 22, 'Bill', '2019-07-17', 'scash', '0', 40000.00, '2019-07-17 00:13:28', '2019-07-17 00:13:28'),
(2, 22, 'Payment', '2019-07-17', 'on', '0', 5000.00, '2019-07-17 00:17:28', '2019-07-17 00:17:28'),
(3, 1, 'Customer Payment', '2019-07-17', 'on', '0', 1000.00, '2019-07-17 00:19:39', '2019-07-17 00:19:39'),
(4, 1, 'Customer Payment', '2019-07-18', 'Cash', '0', 4000.00, '2019-07-18 11:37:08', '2019-07-18 11:37:08'),
(5, 23, 'Bill', '2019-07-23', 'scash', '0', 1000.00, '2019-07-23 03:00:08', '2019-07-23 03:00:08'),
(6, 24, 'Bill', '2019-07-24', 'scash', '0', 5000.00, '2019-07-24 00:45:02', '2019-07-24 00:45:02'),
(7, 25, 'Bill', '2019-07-24', 'scash', '0', 5000.00, '2019-07-24 01:10:25', '2019-07-24 01:10:25'),
(8, 26, 'Bill', '2019-07-24', 'scash', '0', 6000.00, '2019-07-24 02:42:23', '2019-07-24 02:42:23'),
(9, 27, 'Bill', '2019-07-26', 'scash', '0', 3000.00, '2019-07-26 01:59:29', '2019-07-26 01:59:29'),
(10, 28, 'Bill', '2019-07-26', 'scash', '0', 3000.00, '2019-07-26 02:01:43', '2019-07-26 02:01:43'),
(11, 29, 'Bill', '2019-07-26', 'scash', '0', 3000.00, '2019-07-26 02:18:26', '2019-07-26 02:18:26'),
(12, 30, 'Bill', '2019-07-26', 'scash', '0', 2000.00, '2019-07-26 02:21:42', '2019-07-26 02:21:42'),
(13, 31, 'Bill', '2019-07-27', 'scash', '0', 2000.00, '2019-07-27 08:37:59', '2019-07-27 08:37:59'),
(14, 32, 'Bill', '2019-08-03', 'scash', NULL, 1000.00, '2019-08-02 23:33:27', '2019-08-02 23:33:27'),
(15, 33, 'Bill', '2019-08-03', 'scash', '0', 2000.00, '2019-08-02 23:35:59', '2019-08-02 23:35:59'),
(16, 34, 'Bill', '2019-08-03', 'scash', '0', 10000.00, '2019-08-02 23:38:09', '2019-08-02 23:38:09'),
(17, 35, 'Bill', '2019-08-03', 'scash', '0', 5000.00, '2019-08-02 23:39:01', '2019-08-02 23:39:01'),
(18, 31, 'Payment', '2019-08-03', 'cash', '0', 500.00, '2019-08-02 23:58:07', '2019-08-02 23:58:07'),
(19, 9, 'Customer Payment', '2019-08-03', 'Cash', '0', 4000.00, '2019-08-03 00:00:43', '2019-08-03 00:00:43'),
(21, 36, 'Bill', '2019-08-05', 'scash', '0', 8500.00, '2019-08-05 00:29:08', '2019-08-05 00:29:08'),
(22, 22, 'Payment', '2019-08-05', 'cash', '0', 1000.00, '2019-08-05 01:11:57', '2019-08-05 01:11:57'),
(23, 37, 'Bill', '2019-08-05', 'scash', '0', 5000.00, '2019-08-05 05:16:13', '2019-08-05 05:16:13'),
(24, 38, 'Bill', '2019-08-07', 'scash', '0', 10000.00, '2019-08-06 23:20:41', '2019-08-06 23:20:41'),
(25, 39, 'Bill', '2019-08-09', 'scash', '0', 5000.00, '2019-08-09 05:38:50', '2019-08-09 05:38:50'),
(26, 22, 'Payment', '2019-08-09', 'cash', '0', 1000.00, '2019-08-09 06:35:51', '2019-08-09 06:35:51'),
(27, 40, 'Bill', '2020-02-27', 'scash', '0', 1000.00, '2020-02-27 08:59:11', '2020-02-27 08:59:11'),
(28, 40, 'Bill', '2020-02-27', 'scash', '0', 1000.00, '2020-02-27 08:59:12', '2020-02-27 08:59:12');

-- --------------------------------------------------------

--
-- Table structure for table `stock_details`
--

CREATE TABLE `stock_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `material_id` int(11) DEFAULT NULL,
  `cft` double(8,2) NOT NULL,
  `cudate` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock_details`
--

INSERT INTO `stock_details` (`id`, `material_id`, `cft`, `cudate`, `created_at`, `updated_at`) VALUES
(1, 1, 1000.00, '2019-07-17', '2019-07-17 00:11:05', '2019-07-17 00:11:05'),
(2, 2, 0.00, '2019-07-17', '2019-07-17 00:11:05', '2019-07-17 00:11:05'),
(3, 3, 0.00, '2019-07-17', '2019-07-17 00:11:05', '2019-07-17 00:11:05'),
(4, 4, 0.00, '2019-07-17', '2019-07-17 00:11:05', '2019-07-17 00:11:05'),
(5, 5, 0.00, '2019-07-17', '2019-07-17 00:11:05', '2019-07-17 00:11:05'),
(6, 1, 500.00, '2019-07-18', '2019-07-18 06:19:49', '2019-07-18 06:19:49'),
(7, 2, 0.00, '2019-07-18', '2019-07-18 06:19:49', '2019-07-18 06:19:49'),
(8, 3, 0.00, '2019-07-18', '2019-07-18 06:19:49', '2019-07-18 06:19:49'),
(9, 4, 0.00, '2019-07-18', '2019-07-18 06:19:49', '2019-07-18 06:19:49'),
(10, 5, 0.00, '2019-07-18', '2019-07-18 06:19:49', '2019-07-18 06:19:49'),
(11, 1, 500.00, '2019-07-19', '2019-07-18 23:28:02', '2019-07-18 23:28:02'),
(12, 2, 0.00, '2019-07-19', '2019-07-18 23:28:02', '2019-07-18 23:28:02'),
(13, 3, 0.00, '2019-07-19', '2019-07-18 23:28:02', '2019-07-18 23:28:02'),
(14, 4, 0.00, '2019-07-19', '2019-07-18 23:28:02', '2019-07-18 23:28:02'),
(15, 5, 0.00, '2019-07-19', '2019-07-18 23:28:02', '2019-07-18 23:28:02'),
(16, 1, 500.00, '2019-07-22', '2019-07-22 00:08:06', '2019-07-22 00:08:06'),
(17, 2, 0.00, '2019-07-22', '2019-07-22 00:08:06', '2019-07-22 00:08:06'),
(18, 3, 0.00, '2019-07-22', '2019-07-22 00:08:06', '2019-07-22 00:08:06'),
(19, 4, 0.00, '2019-07-22', '2019-07-22 00:08:06', '2019-07-22 00:08:06'),
(20, 5, 0.00, '2019-07-22', '2019-07-22 00:08:06', '2019-07-22 00:08:06'),
(21, 1, 500.00, '2019-07-23', '2019-07-23 00:33:44', '2019-07-23 00:33:44'),
(22, 2, 0.00, '2019-07-23', '2019-07-23 00:33:44', '2019-07-23 00:33:44'),
(23, 3, 0.00, '2019-07-23', '2019-07-23 00:33:44', '2019-07-23 00:33:44'),
(24, 4, 0.00, '2019-07-23', '2019-07-23 00:33:44', '2019-07-23 00:33:44'),
(25, 5, 0.00, '2019-07-23', '2019-07-23 00:33:44', '2019-07-23 00:33:44'),
(26, 1, 500.00, '2019-07-24', '2019-07-23 23:18:36', '2019-07-23 23:18:36'),
(27, 2, 0.00, '2019-07-24', '2019-07-23 23:18:36', '2019-07-23 23:18:36'),
(28, 3, -2000.00, '2019-07-24', '2019-07-23 23:18:36', '2019-07-23 23:18:36'),
(29, 4, 0.00, '2019-07-24', '2019-07-23 23:18:36', '2019-07-23 23:18:36'),
(30, 5, 0.00, '2019-07-24', '2019-07-23 23:18:36', '2019-07-23 23:18:36'),
(31, 1, 500.00, '2019-07-25', '2019-07-24 21:31:05', '2019-07-24 21:31:05'),
(32, 2, -9000.00, '2019-07-25', '2019-07-24 21:31:05', '2019-07-24 21:31:05'),
(33, 3, 1000.00, '2019-07-25', '2019-07-24 21:31:05', '2019-07-24 21:31:05'),
(34, 4, 3000.00, '2019-07-25', '2019-07-24 21:31:05', '2019-07-24 21:31:05'),
(35, 5, 0.00, '2019-07-25', '2019-07-24 21:31:05', '2019-07-24 21:31:05'),
(36, 1, 500.00, '2019-07-26', '2019-07-25 22:48:56', '2019-07-25 22:48:56'),
(37, 2, -9000.00, '2019-07-26', '2019-07-25 22:48:56', '2019-07-25 22:48:56'),
(38, 3, 1000.00, '2019-07-26', '2019-07-25 22:48:56', '2019-07-25 22:48:56'),
(39, 4, 3000.00, '2019-07-26', '2019-07-25 22:48:56', '2019-07-25 22:48:56'),
(40, 5, 0.00, '2019-07-26', '2019-07-25 22:48:56', '2019-07-25 22:48:56'),
(41, 1, 500.00, '2019-07-27', '2019-07-27 01:31:09', '2019-07-27 01:31:09'),
(42, 2, -12000.00, '2019-07-27', '2019-07-27 01:31:09', '2019-07-27 01:31:09'),
(43, 3, 1000.00, '2019-07-27', '2019-07-27 01:31:09', '2019-07-27 01:31:09'),
(44, 4, 0.00, '2019-07-27', '2019-07-27 01:31:09', '2019-07-27 01:31:09'),
(45, 5, 2000.00, '2019-07-27', '2019-07-27 01:31:09', '2019-07-27 01:31:09'),
(46, 1, 500.00, '2019-07-28', '2019-07-27 21:58:01', '2019-07-27 21:58:01'),
(47, 2, 12000.00, '2019-07-28', '2019-07-27 21:58:01', '2019-07-27 21:58:01'),
(48, 3, -1500.00, '2019-07-28', '2019-07-27 21:58:01', '2019-07-27 21:58:01'),
(49, 4, 0.00, '2019-07-28', '2019-07-27 21:58:01', '2019-07-27 21:58:01'),
(50, 5, 2000.00, '2019-07-28', '2019-07-27 21:58:01', '2019-07-27 21:58:01'),
(51, 1, 500.00, '2019-07-29', '2019-07-29 00:27:57', '2019-07-29 00:27:57'),
(52, 2, 12000.00, '2019-07-29', '2019-07-29 00:27:57', '2019-07-29 00:27:57'),
(53, 3, -1500.00, '2019-07-29', '2019-07-29 00:27:57', '2019-07-29 00:27:57'),
(54, 4, 0.00, '2019-07-29', '2019-07-29 00:27:57', '2019-07-29 00:27:57'),
(55, 5, 2000.00, '2019-07-29', '2019-07-29 00:27:57', '2019-07-29 00:27:57'),
(56, 1, 500.00, '2019-07-30', '2019-07-30 12:38:10', '2019-07-30 12:38:10'),
(57, 2, 12000.00, '2019-07-30', '2019-07-30 12:38:10', '2019-07-30 12:38:10'),
(58, 3, -1500.00, '2019-07-30', '2019-07-30 12:38:10', '2019-07-30 12:38:10'),
(59, 4, 0.00, '2019-07-30', '2019-07-30 12:38:10', '2019-07-30 12:38:10'),
(60, 5, 2000.00, '2019-07-30', '2019-07-30 12:38:10', '2019-07-30 12:38:10'),
(61, 1, 500.00, '2019-07-31', '2019-07-30 23:59:55', '2019-07-30 23:59:55'),
(62, 2, 12000.00, '2019-07-31', '2019-07-30 23:59:55', '2019-07-30 23:59:55'),
(63, 3, -1500.00, '2019-07-31', '2019-07-30 23:59:55', '2019-07-30 23:59:55'),
(64, 4, 0.00, '2019-07-31', '2019-07-30 23:59:55', '2019-07-30 23:59:55'),
(65, 5, 2000.00, '2019-07-31', '2019-07-30 23:59:55', '2019-07-30 23:59:55'),
(66, 1, 500.00, '2019-08-01', '2019-08-01 01:21:40', '2019-08-01 01:21:40'),
(67, 2, 12000.00, '2019-08-01', '2019-08-01 01:21:40', '2019-08-01 01:21:40'),
(68, 3, -1500.00, '2019-08-01', '2019-08-01 01:21:40', '2019-08-01 01:21:40'),
(69, 4, 0.00, '2019-08-01', '2019-08-01 01:21:40', '2019-08-01 01:21:40'),
(70, 5, 2000.00, '2019-08-01', '2019-08-01 01:21:40', '2019-08-01 01:21:40'),
(71, 1, 500.00, '2019-08-02', '2019-08-02 01:08:24', '2019-08-02 01:08:24'),
(72, 2, 12000.00, '2019-08-02', '2019-08-02 01:08:24', '2019-08-02 01:08:24'),
(73, 3, -1500.00, '2019-08-02', '2019-08-02 01:08:24', '2019-08-02 01:08:24'),
(74, 4, 0.00, '2019-08-02', '2019-08-02 01:08:24', '2019-08-02 01:08:24'),
(75, 5, 2000.00, '2019-08-02', '2019-08-02 01:08:24', '2019-08-02 01:08:24'),
(76, 1, 500.00, '2019-08-03', '2019-08-02 23:03:34', '2019-08-02 23:03:34'),
(77, 2, 12000.00, '2019-08-03', '2019-08-02 23:03:34', '2019-08-02 23:03:34'),
(78, 3, -1500.00, '2019-08-03', '2019-08-02 23:03:34', '2019-08-02 23:03:34'),
(79, 4, 0.00, '2019-08-03', '2019-08-02 23:03:34', '2019-08-02 23:03:34'),
(80, 5, 2000.00, '2019-08-03', '2019-08-02 23:03:34', '2019-08-02 23:03:34'),
(81, 1, 0.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(82, 2, 10000.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(83, 3, -1500.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(84, 4, -1000.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(85, 5, 2000.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(86, 6, 2000.00, '2019-08-05', '2019-08-05 00:11:37', '2019-08-05 00:11:37'),
(87, 1, -100.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(88, 2, 9000.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(89, 3, -1500.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(90, 4, -1000.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(91, 5, 2000.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(92, 6, 2000.00, '2019-08-07', '2019-08-06 23:09:30', '2019-08-06 23:09:30'),
(93, 1, -100.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(94, 2, 7000.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(95, 3, -1500.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(96, 4, -1000.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(97, 5, 2000.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(98, 6, 2000.00, '2019-08-08', '2019-08-07 18:34:28', '2019-08-07 18:34:28'),
(99, 1, -100.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(100, 2, 7000.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(101, 3, -1500.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(102, 4, -1000.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(103, 5, 2000.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(104, 6, 2000.00, '2019-08-09', '2019-08-09 05:35:18', '2019-08-09 05:35:18'),
(105, 1, -100.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(106, 2, 7000.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(107, 3, -1500.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(108, 4, -1000.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(109, 5, 2000.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(110, 6, 2000.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(111, 7, 4000.00, '2019-10-30', '2019-10-30 10:53:22', '2019-10-30 10:53:22'),
(112, 1, -100.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(113, 2, 7000.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(114, 3, -1500.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(115, 4, -1000.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(116, 5, 2000.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(117, 6, 2000.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(118, 7, 4000.00, '2019-11-09', '2019-11-09 15:06:42', '2019-11-09 15:06:42'),
(119, 1, -100.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(120, 2, 7000.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(121, 3, -1500.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(122, 4, -1000.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(123, 5, 2000.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(124, 6, 2000.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38'),
(125, 7, 4000.00, '2020-02-27', '2020-02-27 08:58:38', '2020-02-27 08:58:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 0, '$2y$10$HjlwUNi/s8bpXh09PCZ68OIQAnD4h4ykpEQACDw0WrEIW3tFZll1W', NULL, '2019-02-27 05:34:45', '2019-02-27 05:34:45');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacity` double(8,2) DEFAULT NULL,
  `descr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `km` double(8,2) DEFAULT NULL,
  `permit` date DEFAULT NULL,
  `tax` date DEFAULT NULL,
  `test` date DEFAULT NULL,
  `insurance` date DEFAULT NULL,
  `ownername` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `number`, `type`, `capacity`, `descr`, `km`, `permit`, `tax`, `test`, `insurance`, `ownername`, `created_at`, `updated_at`) VALUES
(1, 'KL-29A-01', 'type1', 1000.00, '', 10200.00, '2019-09-02', '2019-07-26', '2019-09-02', '2019-09-02', 'Chalavadakathil', '2019-07-12 06:09:23', '2019-07-12 06:09:23'),
(2, 'KL-29S-1993', 'type2', 2000.00, NULL, 12030.00, '2019-07-27', '2019-07-27', '2019-07-27', '2019-07-27', 'Chalavadakathil', '2019-07-12 06:09:57', '2019-07-12 06:09:57'),
(3, 'KL-29M-123', 'type3', 3000.00, 'quote', 0.00, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'owner1', '2019-07-12 06:10:26', '2019-07-12 06:10:26'),
(4, 'KL-23O-369', 'type7', 3000.00, NULL, NULL, NULL, NULL, NULL, NULL, 'owner3', '2019-07-12 06:10:52', '2019-07-12 06:10:52'),
(5, 'KL-36L-2522', 'Lorry', 2000.00, '', 2520.00, '2019-08-31', '2019-08-31', '2019-08-31', '2019-08-31', '', '2019-07-17 00:00:13', '2019-07-17 00:00:13'),
(6, 'KL-96k-253', 'minilorry', 3000.00, NULL, NULL, NULL, NULL, NULL, NULL, 'owner5', '2019-07-17 00:01:12', '2019-07-17 00:01:12'),
(7, 'KL-29L-7845', 'minilory', 3000.00, 'sub roads restricted', 5000.00, '2019-09-02', '2019-12-28', '2019-09-02', '2019-09-30', 'Chalavadakathil', '2019-08-02 09:24:43', '2019-08-02 09:24:43'),
(8, 'KL-29S-8246', 'type 5', 5000.00, '', 20000.00, '2019-08-22', '2019-08-31', '2019-08-30', '2019-08-30', 'Chalavadakathil', '2019-08-02 12:32:53', '2019-08-02 12:32:53'),
(9, 'KL-88-2012', 'lorry', 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, 'Chalavadakathil', '2019-08-02 23:04:10', '2019-08-02 23:04:10'),
(10, 'KL-36-2586', 'lorry', 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, 'owner9', '2019-08-02 23:05:08', '2019-08-02 23:05:08'),
(11, 'kl-02-2012', 'lorry', 1000.00, NULL, 1000.00, '2019-08-09', '2019-08-09', '2019-08-09', '2019-08-09', 'Chalavadakathil', '2019-08-09 05:36:05', '2019-08-09 05:36:05'),
(12, 'kl-10-1000', 'mini-lorry', 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, 'owner', '2019-08-09 05:37:00', '2019-08-09 05:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_maintains`
--

CREATE TABLE `vehicles_maintains` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `mdate` date DEFAULT NULL,
  `descrp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles_maintains`
--

INSERT INTO `vehicles_maintains` (`id`, `vehicle_id`, `mdate`, `descrp`, `amount`, `created_at`, `updated_at`) VALUES
(1, 2, '2019-07-17', 'painting', 3000.00, '2019-07-17 00:06:23', '2019-07-17 00:06:23'),
(2, 1, '2019-08-03', 'painting', 3000.00, '2019-08-02 23:05:45', '2019-08-02 23:05:45'),
(3, 9, '2019-08-09', 'painting', 2000.00, '2019-08-09 05:37:42', '2019-08-09 05:37:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_nums`
--
ALTER TABLE `invoice_nums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_details`
--
ALTER TABLE `loan_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_pays`
--
ALTER TABLE `old_pays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `open_closes`
--
ALTER TABLE `open_closes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchasers`
--
ALTER TABLE `purchasers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_bills`
--
ALTER TABLE `purchase_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rentals`
--
ALTER TABLE `rentals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_bills`
--
ALTER TABLE `sales_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_details`
--
ALTER TABLE `stock_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles_maintains`
--
ALTER TABLE `vehicles_maintains`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `invoice_nums`
--
ALTER TABLE `invoice_nums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `loan_details`
--
ALTER TABLE `loan_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `old_pays`
--
ALTER TABLE `old_pays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `open_closes`
--
ALTER TABLE `open_closes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchasers`
--
ALTER TABLE `purchasers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchase_bills`
--
ALTER TABLE `purchase_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `rentals`
--
ALTER TABLE `rentals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales_bills`
--
ALTER TABLE `sales_bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sales_payments`
--
ALTER TABLE `sales_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `stock_details`
--
ALTER TABLE `stock_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vehicles_maintains`
--
ALTER TABLE `vehicles_maintains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
