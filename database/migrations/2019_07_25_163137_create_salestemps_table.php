<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalestempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salestemps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_no')->nullable();
            $table->date('sdate')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->integer('custid')->nullable();
            $table->integer('vehicleid')->nullable();
            $table->integer('product_id')->nullable();
            $table->float('rate')->nullable();
            $table->float('quantity')->nullable();
            $table->string('desc')->nullable();
            $table->date('billdate')->nullable();
            $table->string('mode')->nullable();
            $table->string('ref')->nullable();
            $table->float('total')->nullable();
            $table->float('amountpayed')->nullable();
            $table->float('amountpay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salestemps');
    }
}
