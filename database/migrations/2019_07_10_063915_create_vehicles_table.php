<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->nullable();
            $table->string('type')->nullable();
            $table->float('capacity')->nullable();
            $table->string('descr')->nullable();
            $table->float('km')->nullable();
            $table->date('permit')->nullable();
            $table->date('tax')->nullable();
            $table->date('test')->nullable();
            $table->date('insurance')->nullable();
            $table->string('ownername')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
