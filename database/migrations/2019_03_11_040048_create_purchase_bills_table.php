<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_no')->nullable();
            $table->date('pdate')->nullable();
            $table->integer('custid')->nullable();
            $table->integer('vehicleid')->nullable();
            $table->integer('product_id')->nullable();
            $table->float('rate')->nullable();
            $table->float('quantity')->nullable();
            $table->float('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_bills');
    }
}
