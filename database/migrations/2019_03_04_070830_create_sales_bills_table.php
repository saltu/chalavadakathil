<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_no')->nullable();
            $table->date('sdate')->nullable();
            $table->integer('custid')->nullable();
            $table->integer('vehicleid')->nullable();
            $table->string('vehiclenum')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('prodname')->nullable();
            $table->float('rate')->nullable();
            $table->float('quantity')->nullable();
            $table->float('extrafare')->nullable();
            $table->float('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_bills');
    }
}
