<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesMaintainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles_maintains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id')->nullable();
            $table->string('descrp')->nullable();
            $table->float('amount')->nullable();
            $table->date('mdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles_maintains');
    }
}
