$(function() {
    $("#myBtn").on('click', function() {
        $("#myModal").css({ "display": "block" });
    });
});
$(function() {
    $(".close").on('click', function() {
        location.reload();
        $("#myModal").css({ "display": "none" });
    });
});


$('.billselect').select2();

$('.billselect').change(function() {
    var value = $(this).val();
    if (value != '') {
        $.ajax({
            type: 'get',
            url: 'psbill/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#pcustid').val(json['custid']);
                $('#pcustname').val(json['custname']);
                $('#psaletot').val(json['total']);
                $('#psalespayed').val(json['amountpay'] + json['amountpayl']);
            }
        });
    } else {
        $('#pcustid').val('');
        $('#pcustname').val('');
        $('#psaletot').val('');
        $('#psalespayed').val('');
    }
});

// $(function(){
//         $(".close").on('click',function(){
//             $("#popup").css({"display":"none"});
//         });
// });

// window.onclick = function(event) {
//     var modal = document.getElementById("myModal");
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }

$('#myBtn').click(function() {
    var view = 0;
    $.ajax({
        type: 'get',
        url: 'vehicle/notify_view',
        data: { view: view },
        success: function(data) {
            var json = $.parseJSON(data);
            var count = json['count'];
            // alert(json['count']);
            if (count == '1') {
                // notification count clearing success
            } else {
                // notification count clearing failed
            }
        }
    })
});

// customer name validation
$("#custname").change(function() {
    var name = $("#custname").val();
    // alert(name);
    $.ajax({
        type: 'get',
        url: 'sales/customer/name',
        data: { name: name },
        success: function(data) {
            var json = $.parseJSON(data);
            var count = json['count'];
            // alert(json['count']);
            if (count == '0') {
                $('#custname').css({ "border-color": "red" });
                $('#content').addClass('display-hide');
                $('#fcontent').addClass('display-hide');
                $('#namealert').removeClass('display-hide');
            } else {
                $('#custname').css({ "border-color": "" });
                $('#content').removeClass('display-hide');
                $('#fcontent').removeClass('display-hide');
                $('#namealert').addClass('display-hide');
            }
        }
    })
});
// Item validation
$("#itemname").change(function() {
    var name = $("#itemname").val();
    // alert(name);
    $.ajax({
        type: 'get',
        url: 'sales/customer/item',
        data: { name: name },
        success: function(data) {
            var json = $.parseJSON(data);
            var count = json['count'];
            // alert(json['count']);
            if (count == '0') {
                $('#itemname').css({ "border-color": "red" });
                $('.form-actions').addClass('display-hide');
                alert("Enter a valid material in stock");
            } else {
                $('#itemname').css({ "border-color": "" });
                $('.form-actions').removeClass('display-hide');
            }
        }
    })
});


$('#creditdate').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        $('.dailyreport2').html("<p>No Data Found</p>");
        var creditdate = $('#creditdate').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: 'daily_creditreport/details',
            data: {
                creditdate: creditdate
            },
            success: function(data) {
                // alert(data);
                $('#dailycredit1').removeClass("display-hide");
                $('#dailycredit1').html("Credit Report -" + creditdate);
                $('#dailycredit2').removeClass("display-hide");
                $('.dailycredit2').html(data);
            }
        });
    }
});

$(function() {
    $("#cudate").on("click", function() {
        var a = $("#cudate").attr("type");
        if (a == "") {
            $("#cudate").attr("type", "date");
        }
    });
});
$(function() {
    $("#pudate").on("click", function() {
        var a = $("#pudate").attr("type");
        if (a == "") {
            $("#pudate").attr("type", "date");
        }
    });
});
$(".im").on("click", function() {
    $('#samplef').removeClass('display-hide');
    $('#sample0').addClass('display-hide');
    $('#sample1').addClass('display-hide');
    $('#sample2').addClass('display-hide');
    $('#sample3').addClass('display-hide');
    $('#sample4').addClass('display-hide');
})
$(".0").on("click", function() {
    $('#samplef').addClass('display-hide');
    $('#sample0').removeClass('display-hide');
    $('#sample1').addClass('display-hide');
    $('#sample2').addClass('display-hide');
    $('#sample3').addClass('display-hide');
    $('#sample4').addClass('display-hide');
})
$(".1").on("click", function() {
    $('#samplef').addClass('display-hide');
    $('#sample0').addClass('display-hide');
    $('#sample1').removeClass('display-hide');
    $('#sample2').addClass('display-hide');
    $('#sample3').addClass('display-hide');
    $('#sample4').addClass('display-hide');
})
$(".2").on("click", function() {
    $('#samplef').addClass('display-hide');
    $('#sample0').addClass('display-hide');
    $('#sample1').addClass('display-hide');
    $('#sample2').removeClass('display-hide');
    $('#sample3').addClass('display-hide');
    $('#sample4').addClass('display-hide');
})
$(".3").on("click", function() {
    $('#samplef').addClass('display-hide');
    $('#sample0').addClass('display-hide');
    $('#sample1').addClass('display-hide');
    $('#sample2').addClass('display-hide');
    $('#sample3').removeClass('display-hide');
    $('#sample4').addClass('display-hide');
})
$(".4").on("click", function() {
    $('#samplef').addClass('display-hide');
    $('#sample0').addClass('display-hide');
    $('#sample1').addClass('display-hide');
    $('#sample2').addClass('display-hide');
    $('#sample3').addClass('display-hide');
    $('#sample4').removeClass('display-hide');
})
$(function() {
    $("#cudate").on("click", function() {
        var a = $("#cudate").attr("type");
        if (a == "") {
            $("#cudate").attr("type", "date");
        }
    });
});

//vehichle Number -- Maintains
$(".vehicle_num").autocomplete({
    source: 'number',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'number/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#vehicle_id').val(json['id']);
            }
        })
    }
});

//Customer Name
$(".custname").autocomplete({
    source: 'sales/customer/custname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'sales/customer/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#custid').val(json['id']);
                $('#phone').val(json['phone']);
            }
        })
    }
});

//Purchaser Name
$(".purname").autocomplete({
    source: 'purchase/purchaser/purname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'purchase/purchaser/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#purid').val(json['id']);
                $('#phone').val(json['phone']);
            }
        })
    }
});

// Sales 

//Material Name -- Sales
$(".itemname").autocomplete({
    source: 'sales/product/prodname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'sales/product/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['cft']);
                $('#prodid').val(json['id']);
                $('#mqty').val(json['cft']);
            }
        })
    }
});

//vehichle Number -- Sales
$(".vehiclenum").autocomplete({
    source: 'sales/number',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'sales/number/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#vehicleid').val(json['id']);
                $('#item-qty').val(json['capacity']);
                $('#vcpty').val(json['capacity']);
            }
        })
    }
});

//sales quantity validation
$('#itemname').change(function() {
    var vqty = $('#vcpty').val();
    var mqty = $('#mqty').val();
    if (parseFloat(mqty) != 0 || parseFloat(mqty) > 0) {
        if (parseFloat(mqty) > parseFloat(vqty)) {
            // alert(mqty);
            $('#item-qty').val(vqty);
        } else {
            // alert(mqty);
            $('#item-qty').val(mqty);
        }
    }
    if (parseFloat(mqty) < 0 || parseFloat(mqty) == 0) {
        // alert(mqty);
        $('#item-qty').val(0);
    }
});

$('#item-rate').on('input', function(e) {
    var rate = $('#item-rate').val();
    var qty = $('#item-qty').val();
    var extfare = $('#extrafare').val();
    var total = $('#item-total').val();

    var totall = ((parseFloat(rate) * parseFloat(qty)) + parseFloat(extfare));

    $('#item-total').val(totall);

});

$('#extrafare').on('input', function(e) {
    var rate = $('#item-rate').val();
    var qty = $('#item-qty').val();
    var total = $('#item-total').val();

    var totall = parseFloat(rate) * parseFloat(qty);
    var extfare = $('#extrafare').val();

    var totalll = parseFloat(totall) + parseFloat(extfare);

    $('#item-total').val(totalll);

})

$('#item-qty').on('input', function(e) {
    var mat = $('#mqty').val();
    var qty = $('#item-qty').val();
    if (mat < parseInt(qty)) {
        $('#submitsalesbil').addClass('display-hide');
        $('#cancelsalesbil').addClass('display-hide');
        $("#alertsalespay").html("The quantity you entered is higher than its stock.");
        $('#alertsalespay').removeClass('display-hide');
    } else {
        $('#alertsalespay').addClass('display-hide');
        $('#submitsalesbil').removeClass('display-hide');
        $('#cancelsalesbil').removeClass('display-hide');
        $("#alertsalespay").html("The Amount you entered is higher.");
    }
    var rate = $('#item-rate').val();
    var extfare = $('#extrafare').val();
    var total = $('#item-total').val();

    var totall = ((parseFloat(rate) * parseFloat(qty)) + parseFloat(extfare));

    $('#item-total').val(totall);

});
// sales bill payment on submission

$('#amountpayed').on('input', function() {
    var salespayed = $('#amountpayed').val();
    var total = $('#item-total').val();
    if (parseFloat(salespayed) > parseFloat(total)) {
        $('#amountpayed').css({ "border-color": "red" });
        $('#submitsalesbil').addClass('display-hide');
        $('#cancelsalesbil').addClass('display-hide');
        $('#alertsalespay').removeClass('display-hide');
    } else {
        $('#amountpayed').css({ "border-color": "" });
        $('#cancelsalesbil').removeClass('display-hide');
        $('#submitsalesbil').removeClass('display-hide');
        $('#alertsalespay').addClass('display-hide');
    }

});

// purchase

//Material Name -- purchase
$(".itemnamep").autocomplete({
    source: 'purchase/product/prodname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'purchase/product/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#prodidp').val(json['id']);
                $('#mqtyp').val(json['cft']);
            }
        })
    }
});

//vehichle Number -- purchase
$(".vehiclenump").autocomplete({
    source: 'purchase/number',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'purchase/number/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#vehicleidp').val(json['id']);
                $('#item-qtyp').val(json['capacity']);
                $('#vcptyp').val(json['capacity']);
            }
        })
    }
});

$('#item-ratep').on('input', function(e) {
    var rate = $('#item-ratep').val();
    var qty = $('#item-qtyp').val();
    var total = $('#item-totalp').val();

    var totall = parseFloat(rate) * parseFloat(qty);

    $('#item-totalp').val(totall);

});

//purchase quantity validation
// $('#itemnamep').change(function(){
//     var vqty = $('#vcptyp').val();
//     var mqty = $('#mqtyp').val();
//     if (parseFloat(mqty) !=  0 || parseFloat(mqty) > 0 ) 
//     {
//         if(parseFloat(mqty) > parseFloat(vqty))
//         {
//             // alert(mqty);
//             $('#item-qtyp').val(vqty);
//         }
//         else
//         { 
//             // alert(mqty);
//             $('#item-qtyp').val(mqty);  
//         }
//     }
//     if (parseFloat(mqty) < 0 || parseFloat(mqty) == 0 )
//     {
//             // alert(mqty);
//             $('#item-qtyp').val(0);
//     }   
// });

$('#item-qtyp').on('input', function(e) {

    var rate = $('#item-ratep').val();
    var qty = $('#item-qtyp').val();
    var total = $('#item-totalp').val();

    var totall = parseFloat(rate) * parseFloat(qty);

    $('#item-totalp').val(totall);

});

//billno -- Payment sales
$(".psbillno").autocomplete({
    source: 'psbill/no',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'psbill/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#pcustid').val(json['custid']);
                $('#pcustname').val(json['custname']);
                $('#psaletot').val(json['total']);
                $('#psalespayed').val(json['amountpay'] + json['amountpayl']);
            }
        })
    }
});


//billno -- Payment Purchase
$(".ppbillno").autocomplete({
    source: 'ppbill/no',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'ppbill/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#ppurid').val(json['ppurid']);
                $('#ppurname').val(json['ppurname']);
                $('#ppurchasetot').val(json['ppurchasetot']);
                $('#ppurchasepayed').val(json['amountpay'] + json['amountpayl']);
            }
        })
    }
});


//sales by cust num
$(".pcustname").autocomplete({
    source: 'customer/custname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'customer/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);   
                $('#psalespay').attr('readonly', false);
                $('#newsubmitsales').removeClass('display-hide');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: 'customer/report/list',
                    data: { value: json['id'] },
                    success: function(data) {
                        // var json = $.parseJSON(data);
                        // alert(data);

                        $('#pcustid').val(json['id']);
                        $('#psaletot').val(json['amount']);
                        $('#psalespay').val(json['amount']);
                        $('#hide-dis').removeClass("display-hide");
                        if (json['amount'] == 0) {
                            $('#psalespay').attr('readonly', true);
                            $('#newsubmitsales').addClass('display-hide');
                        }
                        $('#customerreport1').removeClass("display-hide");
                        $('.customerreport1').html(data);
                    }
                });

            }
        })
    }
});


//purchase by name
$(".ppurname").autocomplete({
    source: 'purchaser/purname',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type: 'get',
            url: 'purchaser/details',
            data: { value: value },
            success: function(data) {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#ppurchasepay').attr('readonly', false);
                $('#newsubmitpur').removeClass('display-hide');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: 'purchaser/report/list',
                    data: { value: json['id'] },
                    success: function(data) {
                        // var json = $.parseJSON(data);
                        // alert(data);
                        $('#ppurid').val(json['id']);
                        $('#ppurchasetot').val(json['amount']);
                        $('#ppurchasepay').val(json['amount']);

                        $('#hide-diss').removeClass("display-hide");

                        if (json['amount'] == 0) {
                            $('#ppurchasepay').attr('readonly', true);
                            $('#newsubmitpur').addClass('display-hide');
                        }
                        $('#purchaserreport1').removeClass("display-hide");
                        $('.purchaserreport1').html(data);
                    }
                });

            }
        })
    }
});

//submission conditions.Sales

$('#psalespay').on('input', function() {
    var psalespay = $('#psalespay').val();
    var amountpayed = $('#psalespayed').val();
    var total = $('#psaletot').val();
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if (parseFloat(psalespay) > bal) {
        // $('#psalespay').attr('readonly', true);
        $('#psalespay').css({ "border-color": "red" });
        $('#newsubmitsales').addClass('display-hide');
        $('#alertsalespa').removeClass('display-hide');
    } else {
        $('#psalespay').css({ "border-color": "" });
        $('#newsubmitsales').removeClass('display-hide');
        $('#alertsalespa').addClass('display-hide');
    }

});

//submission conditions.purchase

$('#ppurchasepay').on('input', function() {
    var ppurchasepay = $('#ppurchasepay').val();
    var amountpayed = $('#ppurchasepayed').val();
    var total = $('#ppurchasetot').val();
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if (parseFloat(ppurchasepay) > bal) {
        // $('#psalespay').attr('readonly', true);
        $('#ppurchasepay').css({ "border-color": "red" });
        $('#newsubmitpur').addClass('display-hide');
        $('#alertpurpa').removeClass('display-hide');
    } else {
        $('#ppurchasepay').css({ "border-color": "" });
        $('#newsubmitpur').removeClass('display-hide');
        $('#alertpurpa').addClass('display-hide');
    }

});

// $('#psalespay').on('input',function() {
//     var psalespay = $('#psalespay').val();
//     var bal = $('#psaletot').val();
//     if( parseFloat(psalespay) > parseFloat(bal)){
//         // $('#psalespay').attr('readonly', true);
//         $('#psalespay').css({"border-color":"red"});
//         $('#newsubmitsales').addClass('display-hide');
//         $('#alertsalespa').removeClass('display-hide');
//     } else {        
//         $('#psalespay').css({"border-color":""});
//         $('#newsubmitsales').removeClass('display-hide');
//         $('#alertsalespa').addClass('display-hide');
//     }

// });

// vehichle owner

$('#ot').on('click', function(e) {
    $('.dis1').addClass('display-hide');
    $('.dis2').addClass('display-hide');
    $('.dis3').addClass('display-hide');
    $('.dis4').addClass('display-hide');
    $('.dis5').addClass('display-hide');

    $('#ownername').attr('readonly', false);
    $('#ownername').val("");
});

$('#cv').on('click', function(e) {
    $('.dis1').removeClass('display-hide');
    $('.dis2').removeClass('display-hide');
    $('.dis3').removeClass('display-hide');
    $('.dis4').removeClass('display-hide');
    $('.dis5').removeClass('display-hide');

    $('#ownername').attr('readonly', true);
    $('#ownername').val("Chalavadakathil");
});

// sales bill Payment


$('#scash').on('click', function(e) {
    $('.sref').addClass('display-hide');
});

$('#scheque').on('click', function(e) {
    $('.sref').removeClass('display-hide');
    $('#sref').val("");
});

// Purchase Bill Payment


$('#pcash').on('click', function(e) {
    $('.pref').addClass('display-hide');
});

$('#pcheque').on('click', function(e) {
    $('.pref').removeClass('display-hide');
    $('#pref').val("");
});



// sales Payment


$('#spcash').on('click', function(e) {
    $('.spreff').addClass('display-hide');
});

$('#spcheque').on('click', function(e) {
    $('.spreff').removeClass('display-hide');
    $('#spref').val("");
});

// sales customer Payment


$('#scpcash').on('click', function(e) {
    $('.scpreff').addClass('display-hide');
});

$('#scpcheque').on('click', function(e) {
    $('.scpreff').removeClass('display-hide');
    $('#scpref').val("");
});




// purchase Payment


$('#ppcash').on('click', function(e) {
    $('.ppreff').addClass('display-hide');
});

$('#ppcheque').on('click', function(e) {
    $('.ppreff').removeClass('display-hide');
    $('#ppref').val("");
});

// purchase purchaser Payment


$('#pppcash').on('click', function(e) {
    $('.pppreff').addClass('display-hide');
});

$('#pppcheque').on('click', function(e) {
    $('.pppreff').removeClass('display-hide');
    $('#pppref').val("");
});


//Loan report
$(".lsource").autocomplete({
    source: 'sourcename',
    minLength: 1,
    select: function(event, ui) {
        value = ui.item.value;
        // alert(ui.item.value);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: 'report/data',
            data: { value: value },
            success: function(data) {
                // var json = $.parseJSON(data);
                // alert(data);
                $('#lreport1').removeClass("display-hide");
                $('#lreport2').removeClass("display-hide");
                $('.lreport2').html(data);
            }
        })
    }
});


// sales report

// $("#sdatefrom").datepicker().datepicker("setDate", new Date());

$('#sdatefrom').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        var datefrom = $('#sdatefrom').val();
        var dateto = $('#sdateto').val();
        if (dateto == '') {
            // $('#sdatefrom').focus();
            $('#sdateto').css({ "border-color": "red" });
        } else {
            $('#sdateto').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'salesreport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    // alert(data);
                    $('#salesreport1').removeClass("display-hide");
                    $('#salesreport2').removeClass("display-hide");
                    $('.salesreport2').html(data);
                }
            });
        }
    }
});


$('#sdateto').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        $('#sdateto').css({ "border-color": "" });
        var datefrom = $('#sdatefrom').val();
        var dateto = $('#sdateto').val();
        if (datefrom == '') {
            // $('#sdatefrom').focus();
            $('#sdatefrom').css({ "border-color": "red" });
            $('#sdateto').val('');
        } else {
            $('#sdatefrom').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'salesreport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    $('#salesreport1').removeClass("display-hide");
                    $('.salesreport2').html(data);
                }
            });
        }
    }
});

// Purchase report


$('#pdatefrom').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        var datefrom = $('#pdatefrom').val();
        var dateto = $('#pdateto').val();
        if (dateto == '') {
            // $('#sdatefrom').focus();
            $('#pdateto').css({ "border-color": "red" });
        } else {
            $('#pdateto').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'purchasereport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    // alert(data);
                    $('#purchasereport1').removeClass("display-hide");
                    $('#purchasereport2').removeClass("display-hide");
                    $('.purchasereport2').html(data);
                }
            });
        }
    }
});
$('#pdateto').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        $('#pdateto').css({ "border-color": "" });
        var datefrom = $('#pdatefrom').val();
        var dateto = $('#pdateto').val();
        if (datefrom == '') {
            // $('#sdatefrom').focus();
            $('#pdatefrom').css({ "border-color": "red" });
            $('#pdateto').val('');
        } else {
            $('#pdatefrom').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'purchasereport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    $('#purchasereport1').removeClass("display-hide");
                    $('.purchasereport2').html(data);
                }
            });
        }
    }
});

// dailyreport

// $("#dailydate").datepicker({ dateFormat: "yy-mm-dd"}).datepicker("setDate", new Date());

$('#dailydate').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        $('.dailyreport2').html("<p>No Data Found</p>");
        var dailydate = $('#dailydate').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: 'report/details',
            data: {
                dailydate: dailydate
            },
            success: function(data) {
                // alert(data);
                $('#dailyreport1').removeClass("display-hide");
                $('#dailyreport2').removeClass("display-hide");
                $('.dailyreport2').html(data);
            }
        });
    }
});

// customreport
$('#curedatefrom').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        var datefrom = $('#curedatefrom').val();
        var dateto = $('#curedateto').val();
        if (dateto == '') {
            // $('#sdatefrom').focus();
            $('#curedateto').css({ "border-color": "red" });
        } else {
            $('#curedateto').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'customreport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    // alert(data);
                    $('#customreport1').removeClass("display-hide");
                    $('#customreport2').removeClass("display-hide");
                    $('.customreport2').html(data);
                }
            });
        }
    }
});
$('#curedateto').datepicker({
    changeMonth: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "1970:+0",
    dateFormat: "yy/mm/dd",
    onSelect: function(dateText) {
        $('#curedateto').css({ "border-color": "" });
        var datefrom = $('#curedatefrom').val();
        var dateto = $('#curedateto').val();
        if (datefrom == '') {
            // $('#sdatefrom').focus();
            $('#curedatefrom').css({ "border-color": "red" });
            $('#curedateto').val('');
        } else {
            $('#curedatefrom').css({ "border-color": "" });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: 'customreport/data',
                data: {
                    datefrom: datefrom,
                    dateto: dateto
                },
                success: function(data) {
                    $('#customreport1').removeClass("display-hide");
                    $('.customreport2').html(data);
                }
            });
        }
    }
});

// Account manage
$('#search_sub').click(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: 'account/bill/details',
        success: function(data) {
            // alert(data);
            $('#search_sub1').removeClass("display-hide");
            // $('#search_sub2').removeClass("display-hide");
            $('#delete_bill').removeClass("display-hide");
            $('.search_sub2').html(data);
        }
    });
});