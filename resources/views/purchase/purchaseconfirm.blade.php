@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Purchase</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route('purchase')}}">Purchase</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Purchase Confirm</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->            
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <div class="caption">Confirm Details</div>

                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('purchase.add') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }} 
                                        <div class="form-body">
                                            <!-- <h3 class="form-section">Info</h3> -->
                                            <div class="row">
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="pudate" name="pudate" readonly value="{{ Session::get('pdate') }}" required placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <br> -->
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="billno" name="billno" readonly value="{{ Session::get('billno') }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <!-- Name -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="hidden" class="form-control purid" id="purid" name="purid" readonly value="{{ Session::get('purid') }}">
                                                            <input type="text" required class="form-control purname" id="purname" name="purname" readonly value="{{ Session::get('purname') }}" placeholder="Purchaser name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" class="form-control phone" id="phone" name="phone"readonly value="{{ Session::get('phone') }}"placeholder="Phone number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="form-section">Materials</h3>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:150px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Vehicle Number </th>
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> CFT </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input type="text" id="vehiclenump" name="vehiclenump" class="form-control form-filter input-sm vehiclenump" readonly value="{{ Session::get('vehiclenump') }}" />
                                                                        <input type="hidden" id="vehicleidp" name="vehicleidp" class="form-control form-filter input-sm vehicleidp" readonly value="{{ Session::get('vehicleidp') }}" />
                                                                        </td>
                                                                        <td><input type="text" id="itemnamep" name="itemnamep" class="form-control form-filter input-sm itemnamep" readonly value="{{ Session::get('itemnamep') }}" />
                                                                        <input type="hidden" id="prodidp" name="prodidp" class="form-control form-filter input-sm prodidp" readonly value="{{ Session::get('itemidp') }}" />
                                                                        </td> 
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="item-ratep" name="itemratep" readonly value="{{ Session::get('ratep') }}"> </td>
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="item-qtyp" name="itemqtyp" readonly value="{{ Session::get('quantityp') }}">
                                                                        </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="item-totalp" name="itemtotalp" readonly value="{{ Session::get('totalp') }}"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{URL::previous()}}"><button type="button" class="btn default">Cancel</button></a>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection