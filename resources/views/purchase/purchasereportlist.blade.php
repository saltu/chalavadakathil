 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex">
            <th> # </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Customer Name </th>
            <th class ="text-center col-md-2"> Vehicle Number </th>
            <th class ="text-center"> Material </th>
            <th class ="text-center"> CFT </th>
            <!-- <th class="text-center"> AmountPayed </th> -->
            <th class="text-center"> Bill Date </th>
            <!-- <th class="text-center"> Action </th> -->
            <!-- <th class="text-center"> Delete</th> -->
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        @if(count($purchase_bill_data) > 0)
            @foreach($purchase_bill_data as $purchase_bill)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $purchase_bill->bill_no }} </td>
                    <?php 
                        $purchaser = DB::table('purchasers')
                                    ->select('*')
                                    ->where('id',$purchase_bill->purid)
                                    ->get();
                    ?>
                    <td> {{ $purchaser[0]->name }} </td>                    
                    <td> {{ $purchase_bill->vehiclenum }} </td>
                    <td> {{ $purchase_bill->prodname }} </td>
                    <td> {{ round($purchase_bill->quantity) }} </td>
                    <!-- <td> {{ round($purchase_bill->amountpayed + $purchase_bill->amountpay) }} </td> -->
                    <td> {{ $pdate=date("d-m-Y",strtotime($purchase_bill->pdate)) }} </td>
                </tr>
            @endforeach
        @endif 
    </tbody>
</table>