@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sales</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a>Sales</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a>Customer</a>
                        <i class="fa fa-circle"></i>
                    </li>

                    <li>
                        <a>Payment</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('payment.customer.bill.add') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <h3 class="form-section">Payment</h3>
                                            <div class="row">  
                                                <div class="col-md-12">                                              
                                                    <!-- Name -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Customer Name</label>
                                                            <div class="col-md-6">
                                                                 <input type="hidden" class="form-control pcustid" id="pcustid" name="pcustid" value="">
                                                                <input type="text" required class="form-control pcustname" id="pcustname" name="pcustname" value="" placeholder="Customer name">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>                                                
                                            </div>
                                            <!--/row-->  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="portlet-body ">
                                                        <div class="customerreport1 display-hide" id="customerreport1"></div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <div class="display-hide hide-dis" id="hide-dis">                                                
                                                <div class="row">
                                                    <div class="col-md-12">                                                    
                                                        <!-- Amount -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Balance Amount</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" readonly class="form-control psaletot" id="psaletot" name="psaletot" placeholder="Total Amount">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Amount Pay</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" required class="form-control psalespay" id="psalespay" name="psalespay" placeholder="Amount">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Cash</label>
                                                            <div class="col-md-3">
                                                                <input type="radio" class="scpcash" value="Cash" checked id="scpcash" name="scpmode">
                                                            </div>
                                                            <label class="control-label col-md-3">Cheque</label>
                                                            <div class="col-md-3">
                                                                <input type="radio" class="scpcheque" value="Cheque" id="scpcheque" name="scpmode">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 display-hide scpreff">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Cheque Number</label>
                                                            <div class="col-md-9">
                                                                <input type="text" value="0" class="form-control scpref" id="scpref" name="scpref" placeholder="Cheque Number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                      
                                            <center>
                                            <div class="form-actions">
                                                <button type="submit" id="newsubmitsales" class="btn green newsubmitsales">Submit</button>
                                                
                                                <div class="row">                                    
                                                    <div class="alert alert-danger display-hide" id="alertsalespa">
                                                        <button class="close" data-close="alert"></button> The Amount you entered is higher. 
                                                    </div>
                                                </div>
                                            </div>
                                            </center>                                           
                                            </div>   
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection