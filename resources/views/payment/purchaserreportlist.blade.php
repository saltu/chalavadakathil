 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> # </th>
            <th class="text-center"> Date </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Amount </th>
            <th class="text-center"> Type </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $drtot = 0;
            $crtot = 0;
        ?>
        <!-- purchases bill -->
        @if(count($purchases_bills) > 0)
            @foreach($purchases_bills as $purchases_bill)
                <tr>
                    <td> Bill </td>
                    <td> {{ $pdate=date("d-m-Y",strtotime($purchases_bill->pdate)) }} </td>
                    <td> {{ $purchases_bill->bill_no }} </td>
                    <td> {{ $purchases_bill->total }} </td>
                    <td> Dr </td>
                    <?php $drtot += $purchases_bill->total; ?>
                </tr>
            @endforeach
        @endif

        <!-- purchases bill -->
        @if(count($purchases_bills_payments) > 0)
            @foreach($purchases_bills_payments as $purchases_bills_payment)
                <tr>
                    <td> Payment By Purchaser </td>
                    <td colspan="2" class="text-center"> {{ $billdate=date("d-m-Y",strtotime($purchases_bills_payment->billdate)) }} </td>
                    <td> {{ $purchases_bills_payment->amount }} </td>
                    <td> Cr </td>
                    <?php $crtot += $purchases_bills_payment->amount; ?>
                </tr>
            @endforeach
        @endif

        <!-- purchases bill -->
        <?php 
        $purchases_bills_purs = DB::table('purchase_bills')
                            ->select('*')
                            ->where('purid',$value)
                            ->get();
        ?>  
        @foreach($purchases_bills_purs as $purchases_bills_pur)
        <?php 
        $purchases_bills_pus = DB::table('purchase_payments')
                            ->select('*')
                            ->where('billno',$purchases_bills_pur->bill_no)
                            ->get();
        ?>
            @foreach($purchases_bills_pus as $purchases_bills_pu)
                <tr>
                    <td> Payment By Purchaser By Bill </td>
                    <td colspan="2" class="text-center"> {{ $billdate=date("d-m-Y",strtotime($purchases_bills_pu->billdate)) }} </td>
                    <td> {{ $purchases_bills_pu->amount }} </td>
                    <td> Cr </td>
                    <?php $crtot += $purchases_bills_pu->amount; ?>
                </tr>
            @endforeach
        @endforeach

         <tr>
            <td colspan="2" class="text-right">Total Amount in Bill</td>
            <td colspan="3">{{ $drtot }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Total Amount Payed</td>
            <td colspan="3">{{ $crtot }}</td>
        </tr>
    </tbody>
</table>