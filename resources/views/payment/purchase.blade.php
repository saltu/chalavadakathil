@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Purchase</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Purchase</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Payment</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('payment.purchase.bill.add') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <h3 class="form-section">Payment</h3>
                                            <div class="row">                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill No</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control ppbillno" id="ppbillno" name="ppbillno" placeholder="Bill No">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchaser Name</label>
                                                        <div class="col-md-9">
                                                             <input type="hidden" class="form-control ppurid" id="ppurid" name="ppurid">
                                                            <input type="text" readonly class="form-control purtname" id="ppurname" name="ppurname" value="" placeholder="Purchaser name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Total Amount</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly class="form-control ppurchasetot" id="ppurchasetot" name="ppurchasetot" placeholder="Total Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                            
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Amount Payed</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="" class="form-control ppurchasepayed" id="ppurchasepayed" name="ppurchasepayed" placeholder="Amount Payed">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Amount Pay</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control ppurchasepay" id="ppurchasepay" name="ppurchasepay" placeholder="Amount">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Cash</label>
                                                        <div class="col-md-3">
                                                            <input type="radio" class="ppcash" checked id="ppcash" name="ppmode">
                                                        </div>
                                                        <label class="control-label col-md-3">Cheque</label>
                                                        <div class="col-md-3">
                                                            <input type="radio" class="ppcheque" id="ppcheque" name="ppmode">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--/row-->     
                                            <br>
                                            <div class="row display-hide ppreff">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Cheque Number</label>
                                                        <div class="col-md-9">
                                                            <input type="text" value="0" class="form-control ppref" id="ppref" name="ppref" placeholder="Cheque Number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </div>                                        
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" id="newsubmitpur" class="btn green newsubmitpur">Submit</button>
                                           
                                            <div class="row">                                    
                                                <div class="alert alert-danger display-hide" id="alertpurpa">
                                                    <button class="close" data-close="alert"></button> The Amount you entered is higher. 
                                                </div>
                                            </div>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection