 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> # </th>
            <th class="text-center"> Date </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Material </th>
            <th class="text-center"> Amount </th>
            <th class="text-center"> Type </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $drtot = 0;
            $crtot = 0;
        ?>
        <!-- sales bill -->
        @if(count($sales_bills) > 0)
            @foreach($sales_bills as $sales_bill)
                <tr>
                    <td> Bill </td>
                    <td> {{ $sdate=date("d-m-Y",strtotime($sales_bill->sdate)) }} </td>
                    <td> {{ $sales_bill->bill_no }} </td>
                    <td> {{ $sales_bill->prodname }} </td>
                    <td> {{ $sales_bill->total }} </td>
                    <td> Dr </td>
                    <?php $drtot += $sales_bill->total; ?>
                </tr>
            @endforeach
        @endif

        <!-- sales bill -->
        @if(count($sales_bills_payments) > 0)
            @foreach($sales_bills_payments as $sales_bills_payment)
                <tr>
                    <td> Payment By Customer </td>
                    <td colspan="3" class="text-left"> {{ $billdate=date("d-m-Y",strtotime($sales_bills_payment->billdate)) }} </td>
                    <td> {{ $sales_bills_payment->amount }} </td>
                    <td> Cr </td>
                    <?php $crtot += $sales_bills_payment->amount; ?>
                </tr>
            @endforeach
        @endif

        <tr>
            <td class="text-center warning" colspan="6">Payments</td>
        </tr>

        <!-- sales bill -->
        <?php 
        $sales_bills_custs = DB::table('sales_bills')
                            ->select('*')
                            ->where('custid',$value)
                            ->get();
        ?>  
        @foreach($sales_bills_custs as $sales_bills_cust)
        <?php 
        $sales_bills_cus = DB::table('sales_payments')
                            ->select('*')
                            ->where('billno',$sales_bills_cust->bill_no)
                            ->get();
        ?>
            @foreach($sales_bills_cus as $sales_bills_cu)
                <tr>
                    <td> Payment By Customer By Bill </td>
                    <td  class="text-left"> {{ $billdate=date("d-m-Y",strtotime($sales_bills_cu->billdate)) }} </td>
                    <td colspan="2" class="text-left"> {{ $sales_bills_cu->billno }} </td>
                    <td> {{ $sales_bills_cu->amount }} </td>
                    <td> Cr </td>
                    <?php $crtot += $sales_bills_cu->amount; ?>
                </tr>
            @endforeach
        @endforeach

         <tr>
            <td colspan="2" class="text-right">Total Amount in Bill</td>
            <td colspan="4">{{ $drtot }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-right">Total Amount Payed</td>
            <td colspan="4">{{ $crtot }}</td>
        </tr>
    </tbody>
</table>