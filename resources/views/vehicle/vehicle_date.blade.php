@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Vehicle
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Vehicle</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Vehicle Details</span>
                            </div>
                            <!-- <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div> -->
                        </div>
                        <div class="portlet-body">                            
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Vehicle Number </th>
                                        <th> Permit Date </th>
                                        <th> Tax Date </th>
                                        <th> Test Date </th>
                                        <th> Insurance Date </th>
                                        <th> Owner Name </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($vehicles) > 0 )
                                        @foreach ($vehicles as $vehicle)
                                            <tr id="{{ $vehicle->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ $vehicle->number }} </td>
                                                <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($vehicle->permit)) }} </td>
                                                <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($vehicle->tax)) }} </td>
                                                <td class="text-center"> {{ $test=date("d-m-Y", strtotime($vehicle->test)) }} </td>
                                                <td class="text-center"> {{ $insurance=date("d-m-Y", strtotime($vehicle->insurance)) }} </td>
                                                <td class="text-center"> {{ $vehicle->ownername }} </td>
                                                <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $vehicle->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deleteveh" href="{{ route('vehicle.delete', $vehicle->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection