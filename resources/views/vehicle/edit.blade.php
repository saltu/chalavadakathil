@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Vehicle
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Vehicle</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Vehicle Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{ route('vehicle.add') }}" method="POST" class="form-horizontal">            
                                                {{ csrf_field() }}    
                                                <input type="hidden" value="{{ $vehicles[0]->id }}" name="id" id="id">                               
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Vehicle Number</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" value="{{ $vehicles[0]->number }}" name="number" id="number" placeholder="Enter Vehicle Number">
                                                        </div>
                                                    </div>                                                      
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Vehicle Type</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" value="{{ $vehicles[0]->type }}" name="type" id="type" placeholder="Enter Vehicle Type">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">CFT</label>
                                                        <div class="col-md-4">
                                                            <input type="number" required class="form-control" value="{{ $vehicles[0]->capacity }}" name="capacity" id="capacity" placeholder="Enter CFT">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Description</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" value="{{ $vehicles[0]->descr }}" name="desc" id="desc" placeholder="Enter Description">
                                                        </div>
                                                    </div>   
                                                    <?php 
                                                    if($vehicles[0]->ownername == 'Chalavadakathil') {
                                                    ?>                                                 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">KiloMeter</label>
                                                        <div class="col-md-4">
                                                            <input type="number" class="form-control" value="{{ $vehicles[0]->km }}" name="km" id="km" placeholder="Enter KiloMeter">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Permit Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" value="{{ $vehicles[0]->permit }}" name="permit" id="permit" placeholder="Enter Permit Date">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Tax Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" value="{{ $vehicles[0]->tax }}" name="tax" id="tax" placeholder="Enter Tax Date">
                                                        </div>
                                                    </div>                                                        
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Test Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" value="{{ $vehicles[0]->test }}" name="test" id="test" placeholder="Enter Test Date">
                                                        </div>
                                                    </div>    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Insurance Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" value="{{ $vehicles[0]->insurance }}" name="insurance" id="insurance" placeholder="Enter Insurance Date">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Owner Name</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" readonly value="{{ $vehicles[0]->ownername }}" name="ownername" id="ownername" placeholder="Enter OwnerName">
                                                        </div>
                                                    </div> 
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php 
                                                    if($vehicles[0]->ownername != 'Chalavadakathil') {
                                                    ?>                                               
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Owner Name</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" value="{{ $vehicles[0]->ownername }}" name="ownername" id="ownername" placeholder="Enter OwnerName">
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>

                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('vehicle') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection