@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Vehicle
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Vehicle</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Vehicle Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{ route('vehicle.maintain.add') }}" method="POST" class="form-horizontal">            
                                                {{ csrf_field() }}                                   
                                                <div class="form-body">
                                                    <div class="form-group ">
                                                        <label class="col-md-3 control-label">Vehicle Number</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control vehicle_num" name="vehicle_num" id="vehicle_num" placeholder="Enter Vehicle Number">
                                                            <input type="hidden" name="vehicle_id" id="vehicle_id">
                                                        </div>
                                                    </div>                                                      
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Maintain Descrption</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" name="descrp" id="descrp" placeholder="Enter Maintainces Details">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Amount</label>
                                                        <div class="col-md-4">
                                                            <input type="number" required class="form-control" name="amount" id="amount" placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('home') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Vehicle Number </th>
                                        <th> Date </th>
                                        <th> Vehicle Descrption </th>
                                        <th> Amount </th>
                                        <!-- <th> Edit </th> -->
                                        <!-- <th> Delete </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($vehicle_maintains) > 0 )
                                        @foreach ($vehicle_maintains as $vehicle_maintain)
                                            <tr id="{{ $vehicle_maintain->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <?php 
                                                    $vehicle_numm = DB::table('vehicles')
                                                                ->select('*')
                                                                ->where('id',$vehicle_maintain->vehicle_id)
                                                                ->get();
                                                ?>
                                                <td class="text-center"> {{ $vehicle_numm[0]->number }} </td>
                                                <td class="text-center"> {{ $mdate=date("d-m-Y",strtotime($vehicle_maintain->mdate)) }} </td>
                                                <td class="text-center"> {{ $vehicle_maintain->descrp }} </td>
                                                <td class="text-center"> {{ $vehicle_maintain->amount }} </td>
                                                <!-- <td class="text-center"><a class="editveh" href="{{ route('vehicle.maintain.edit', $vehicle_maintain->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deleteveh" href="{{ route('vehicle.maintain.delete', $vehicle_maintain->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td> -->
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection