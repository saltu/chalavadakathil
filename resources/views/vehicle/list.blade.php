@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Vehicle
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Vehicle</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Vehicle Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{ route('vehicle.add') }}" method="POST" class="form-horizontal">            
                                                {{ csrf_field() }}                                   
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <strong>
                                                            <label class="col-md-3 control-label"></label>
                                                            <input type="radio" checked name="owner" id="cv" value="cv" class="control-label owner">
                                                            Chalavadakathil
                                                            <input type="radio" name="owner" id="ot" value="ot" class="control-label owner">
                                                            Others
                                                        </strong>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Vehicle Number</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" name="number" id="number" placeholder="Enter Vehicle Number">
                                                        </div>
                                                    </div>                                                      
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Vehicle Type</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control" name="type" id="type" placeholder="Enter Vehicle Type">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">CFT</label>
                                                        <div class="col-md-4">
                                                            <input type="number" required class="form-control" name="capacity" id="capacity" placeholder="Enter CFT">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group dis1">
                                                        <label class="col-md-3 control-label">KiloMeter</label>
                                                        <div class="col-md-4">
                                                            <input type="number" class="form-control" name="km" id="km" placeholder="Enter KiloMeter">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Description</label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="desc" id="desc" placeholder="Enter Description">
                                                        </div>
                                                    </div>                                                      
                                                    <div class="form-group dis2">
                                                        <label class="col-md-3 control-label">Permit Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" name="permit" id="permit" placeholder="Enter Permit Date">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group dis3">
                                                        <label class="col-md-3 control-label">Tax Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" name="tax" id="tax" placeholder="Enter Tax Date">
                                                        </div>
                                                    </div>                                                        
                                                    <div class="form-group dis4">
                                                        <label class="col-md-3 control-label">Test Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" name="test" id="test" placeholder="Enter Test Date">
                                                        </div>
                                                    </div>    
                                                    <div class="form-group dis5">
                                                        <label class="col-md-3 control-label">Insurance Date</label>
                                                        <div class="col-md-4">
                                                            <input type="date" class="form-control" name="insurance" id="insurance" placeholder="Enter Insurance Date">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Owner Name</label>
                                                        <div class="col-md-4">
                                                            <input type="text" readonly value="Chalavadakathil" required class="form-control" name="ownername" id="ownername" placeholder="Enter OwnerName">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('home') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h1>Chalavadakathil</h1>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Vehicle Number </th>
                                        <th> Vehicle Type </th>
                                        <th> CFT </th>
                                        <th> KiloMeter </th>
                                        <th> Description </th>
                                        <th> Permit Date </th>
                                        <th> Tax Date </th>
                                        <th> Test Date </th>
                                        <th> Insurance Date </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($vehicles) > 0 )
                                        @foreach ($vehicles as $vehicle)
                                            <tr id="{{ $vehicle->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ $vehicle->number }} </td>
                                                <td class="text-center"> {{ $vehicle->type }} </td>
                                                <td class="text-center"> {{ $vehicle->capacity }} </td>
                                                <td class="text-center"> {{ $vehicle->km }} </td>
                                                <td class="text-center"> {{ $vehicle->descr }} </td>
                                                <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($vehicle->permit)) }} </td>
                                                <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($vehicle->tax)) }} </td>
                                                <td class="text-center"> {{ $test=date("d-m-Y", strtotime($vehicle->test)) }} </td>
                                                <td class="text-center"> {{ $insurance=date("d-m-Y", strtotime($vehicle->insurance)) }} </td>
                                                <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $vehicle->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deleteveh" href="{{ route('vehicle.delete', $vehicle->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table> 
                            <h1>Others</h1>
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Vehicle Number </th>
                                        <th> Vehicle Type </th>
                                        <th> CFT </th>
                                        <th> Description </th>
                                        <th> Owner Name </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($vehicleess) > 0 )
                                        @foreach ($vehicleess as $vehicles)
                                            <tr id="{{ $vehicles->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ $vehicles->number }} </td>
                                                <td class="text-center"> {{ $vehicles->type }} </td>
                                                <td class="text-center"> {{ $vehicles->capacity }} </td>
                                                <td class="text-center"> {{ $vehicles->descr }} </td>
                                                <td class="text-center"> {{ $vehicles->ownername }} </td>
                                                <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $vehicles->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deleteveh" href="{{ route('vehicle.delete', $vehicles->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection