@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Expense
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Expense</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Expense Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{ route('expense.add') }}" method="POST" class="form-horizontal">            
                                                {{ csrf_field() }}                                   
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Date</label>
                                                        <div class="col-md-4">
                                                            <?php 
                                                                $month = date('m');
                                                                $day = date('d');
                                                                $year = date('Y');

                                                                $today = $year . '-' . $month . '-' . $day;
                                                            ?>
                                                            <input type="date" min="<?php echo $today; ?>" value="<?php echo $today; ?>" required class="form-control input-circle" name="exdate" id="exdate" placeholder="Enter Date">
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Description</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control input-circle" name="exdescrip" id="exdescrip" placeholder="Enter Description">
                                                        </div>
                                                    </div>    
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Dr</label>
                                                        <div class="col-md-1">
                                                            <input type="radio" checked required name="type" id="dr" value="Dr">
                                                        </div>
                                                        <label class="col-md-1 control-label">Cr</label>
                                                        <div class="col-md-1">
                                                            <input type="radio" required name="type" id="cr" value="Cr">
                                                        </div>
                                                    </div>                                                  
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Amount</label>
                                                        <div class="col-md-4">
                                                            <input type="number" required class="form-control input-circle" name="examount" id="examount" placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('home') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Date </th>
                                        <th> Description </th>
                                        <th> Type </th>
                                        <th> Amount </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($expenses1) > 0 )
                                        @foreach ($expenses1 as $expense1)
                                            <tr id="{{ $expense1->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ date("d-m-Y",strtotime($expense1->ex_date)) }} </td>
                                                <td class="text-center"> {{ $expense1->description }} </td>
                                                <td class="text-center"> {{ $expense1->type }} </td>
                                                <td class="text-center"> {{ $expense1->amount }} </td>
                                                <td class="text-center"><a class="editcust" href="{{ route('expense.edit', $expense1->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletecust" href="{{ route('expense.delete', $expense1->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>      
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Date </th>
                                        <th> Description </th>
                                        <th> Type </th>
                                        <th> Amount </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($expenses2) > 0 )
                                        @foreach ($expenses2 as $expense2)
                                            <tr id="{{ $expense1->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ date("d-m-Y",strtotime($expense2->ex_date)) }} </td>
                                                <td class="text-center"> {{ $expense2->description }} </td>
                                                <td class="text-center"> {{ $expense2->type }} </td>
                                                <td class="text-center"> {{ $expense2->amount }} </td>
                                                <td class="text-center"><a class="editcust" href="{{ route('expense.edit', $expense2->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deletecust" href="{{ route('expense.delete', $expense2->id ) }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection