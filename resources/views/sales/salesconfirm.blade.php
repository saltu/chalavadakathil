@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sale</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route('sales')}}">Sales</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Sales Confirm</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->            
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <div class="caption">Confirm Details</div>
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('sales.add') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }} 
                                        <div class="form-body">
                                           <!--  <h3 class="form-section">Info</h3> -->
                                            <div class="row">
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" id="ccudate" name="cudate" readonly value="{{ Session::get('sdate') }}" required placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <br> -->
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="billno" name="billno" readonly value="{{ Session::get('billno') }}" placeholder="billno">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                <!-- Name -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="hidden" class="form-control custid" id="custid" name="custid" readonly value="{{ Session::get('custid') }}">
                                                            <input type="text" required class="form-control custname" id="custname" name="custname" readonly value="{{ Session::get('name') }}"  placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" class="form-control phone" id="phone" name="phone" readonly value="{{ Session::get('phone') }}" placeholder="Phone number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- </div> -->
                                            <!--/row-->
                                            
                                            <!-- <h3 class="form-section">Materials</h3> -->
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:150px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Vehicle Number </th>
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> CFT </th>
                                                                        <th class="text-center"> Extra Fare </th>
                                                                        <th class="text-center"> Total </th>
                                                                        <th class="text-center"> AmountPayed </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input type="text" id="vehiclenum" name="vehiclenum" readonly value="{{ Session::get('vehiclenum') }}" class="form-control form-filter input-sm vehiclenum" />
                                                                        <input type="hidden" id="vehicleid" name="vehicleid" readonly value="{{ Session::get('vehicleid') }}" class="form-control form-filter input-sm vehicleid" />
                                                                        </td>
                                                                        <td><input type="text" id="itemname" name="itemname" readonly value="{{ Session::get('itemname') }}" class="form-control form-filter input-sm itemname" />
                                                                        <input type="hidden" id="prodid" name="prodid" readonly value="{{ Session::get('itemid') }}" class="form-control form-filter input-sm prodid" />
                                                                        </td> 
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="item-rate" name="itemrate" readonly value="{{ Session::get('rate') }}"> </td>
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="item-qty" name="itemqty" readonly value="{{ Session::get('quantity') }}">
                                                                        </td>
                                                                        <td> <input required type="text" class="form-control form-filter input-sm" id="extrafare" name="extrafare" readonly value="{{ Session::get('extrafare') }}"> </td>
                                                                        <td> <input required type="text" class="form-control form-filter input-sm" id="item-total" name="itemtotal" readonly value="{{ Session::get('total') }}"> </td>
                                                                        <td> <input required type="text" class="form-control form-filter input-sm" id="amountpayed" name="amountpayed" readonly value="{{ Session::get('amountpayed') }}"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input type="radio" checked value="scash" id="scash" name="smode" class="scash" /> CASH
                                                                        </td>
                                                                        <td><input type="radio" value="scheque" id="scheque" name="smode" class="scheque" /> CHEQUE
                                                                        </td> 
                                                                        <td> <input placeholder="Cheque Number" type="text" class="sref display-hide form-control form-filter input-sm" value="0" id="sref" name="sref"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>                                                            
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green">Confirm</button>
                                            <a href="{{URL::previous()}}"><button type="button" class="btn default">Cancel</button></a>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection