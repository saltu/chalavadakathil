@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sale</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Sales</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->            
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    @if($errors->any())
                                        <b><h4 style="color: red; text-align: center;">{{ $errors->first() }}</h4></b>
                                    @endif
                                    <form action="{{ route('sales.confirm') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }} 
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <?php 
                                                                $month = date('m');
                                                                $day = date('d');
                                                                $year = date('Y');

                                                                $today = $year . '-' . $month . '-' . $day;
                                                            ?>
                                                            <input type="date" min="<?php echo $today; ?>" value="<?php echo $today; ?>" class="form-control" id="cudate" name="cudate" required placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>

                                                 <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="billno" name="billno" readonly value="{{ $innum }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <br>
                                            <hr>
                                            <div class="row">
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <!-- <input type="hidden" class="form-control custid" id="custid" name="custid"> -->
                                                            <input type="text" required class="form-control custname" id="custname" value="{{ old('custname') }}" name="custname" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- Customer ID  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">ID</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required readonly class="form-control custid" id="custid" name="custid" placeholder="Customer ID">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" readonly value="0" class="form-control phone" id="phone" name="phone" value="{{ old('phone') }}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->

                                            <h3 id="namealert" class="text-center font-red display-hide">Please enter registered customer or else go and register new customer</h3>
                                            
                                            <h3 id="fcontent" class="form-section">Materials</h3>
                                            <!--/row-->
                                            <div id="content" class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:150px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Vehicle Number </th>
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class="text-center"> Rate </th>
                                                                        <th class="text-center"> CFT </th>
                                                                        <th class="text-center"> Extra Fare </th>
                                                                        <th class="text-center"> Total </th>
                                                                        <th class="text-center"> AmountPayed </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input required type="text" id="vehiclenum" name="vehiclenum" value="{{ old('vehiclenum') }}" class="form-control form-filter input-sm vehiclenum" />
                                                                        <input type="hidden" value="{{ old('vehicleid') }}" id="vehicleid" name="vehicleid" class="form-control form-filter input-sm vehicleid" />
                                                                        <input type="hidden" id="vcpty" name="vcpty" class="form-control form-filter input-sm vcpty" />
                                                                        </td>
                                                                        <td><input required type="text" id="itemname" name="itemname" value="{{ old('itemname') }}" class="form-control form-filter input-sm itemname" />
                                                                        <input type="hidden" value="{{ old('prodid') }}" id="prodid" name="prodid" class="form-control form-filter input-sm prodid" />
                                                                        <input type="hidden" id="mqty" name="mqty" class="form-control form-filter input-sm mqty" />
                                                                        </td> 
                                                                        <td> <input required type="number" step="0.01" min="0" class="form-control form-filter input-sm" id="item-rate" value="{{ old('itemrate') }}" name="itemrate"> </td>
                                                                        <td> <input required type="number" step="0.01" min="0" class="form-control form-filter input-sm" id="item-qty" value="{{ old('itemqty') }}" name="itemqty">
                                                                        </td>
                                                                        <td> <input required type="number" value="0" class="form-control form-filter input-sm" id="extrafare" value="{{ old('extrafare') }}" name="extrafare"> </td>
                                                                        <td> <input required type="text" value="0" readonly class="form-control form-filter input-sm" value="{{ old('itemtotal') }}" id="item-total" name="itemtotal"> </td>
                                                                        <td> <input required type="text" class="form-control form-filter input-sm" id="amountpayed" value="{{ old('amountpayed') }}" name="amountpayed"> </td>
                                                                    </tr>
                                                                    <!-- <tr>
                                                                        <td><input type="radio" checked value="scash" id="scash" name="smode" class="scash" /> CASH
                                                                        </td>
                                                                        <td><input type="radio" value="scheque" id="scheque" name="smode" class="scheque" /> CHEQUE
                                                                        </td> 
                                                                        <td> <input placeholder="Cheque Number" type="text" class="sref display-hide form-control form-filter input-sm" value="0" id="sref" name="sref"> </td>
                                                                    </tr> -->
                                                                </tbody>
                                                            </table>                                                            
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" id="submitsalesbil" class="btn green ">Submit</button>
                                            <button type="button" id="cancelsalesbil" class="btn default ">Cancel</button>

                                            <div class="row">                                    
                                                <div class="alert alert-danger display-hide" id="alertsalespay">
                                                    <button class="close" data-close="alert"></button> The Amount you entered is higher. 
                                                </div>
                                            </div>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection