 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex" >
            <th> # </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Customer Name </th>
            <th class ="text-center col-md-2"> Vehicle Number </th>
            <th class ="text-center"> Material </th>
            <th class ="text-center"> Total </th>
            <th class="text-center"> AmountPayed </th>
            <th class="text-center"> Bill Date </th>
            <!-- <th class="text-center"> Action </th> -->
            <!-- <th class="text-center"> Delete</th> -->
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        @if(count($sales_bill_data) > 0)
            @foreach($sales_bill_data as $sales_bill)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $sales_bill->bill_no }} </td>
                    <?php 
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_bill->custid)
                                    ->get();
                    ?>
                    <td> {{ $customers[0]->name }} </td>
                    <td> {{ $sales_bill->vehiclenum }} </td>
                    <td> {{ $sales_bill->prodname }} </td>
                    <td> {{ round($sales_bill->total) }} </td>
                    <td> {{ round($sales_bill->amountpayed + $sales_bill->amountpay) }} </td>
                    <td> {{ $sdate=date("d-m-Y",strtotime($sales_bill->sdate)) }} </td>
                </tr>
            @endforeach
        @endif 
    </tbody>
</table>