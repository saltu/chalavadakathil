 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr>
            <th class="text-center"> Sl.No </th>
            <th class="text-center"> Customer Name </th>
            <th class="text-center"> Phone Number </th>
            <th class="text-center"> Product </th>
            <th class="text-center"> Total Amount </th>
            <th class="text-center"> Total Amount Payed </th>
            <th class="text-center"> Pending Amount </th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; $totbaln =0; ?>
        @if(count($credit_data) > 0 )
            @foreach ($credit_data as $credits)
            <?php 
            $total = $credits->total;
            $payed = $credits->amountpayed;
            $cpayed = $credits->amountpay;
            $am_tot_payd = $payed + $cpayed;
            $balance = $total - $am_tot_payd;
            if($balance > 0){
            ?>
                <tr id="{{ $credits->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <?php 
                    $cust_name = DB::table('customers')
                                    ->select('name','phone')
                                    ->where('id',$credits->custid)
                                    ->get();
                    ?>
                    <td class="text-center"> {{ $cust_name[0]->name }} </td>
                    <td class="text-center"> {{ $cust_name[0]->phone }} </td>
                    <td class="text-center"> {{ $credits->prodname }} </td>
                    <td class="text-center"> {{ $total }} </td>
                    <td class="text-center"> {{ $am_tot_payd }} </td>
                    <td class="text-center"> {{ $balance }} </td>
                </tr>
            <?php $totbaln +=$balance;
             } ?>
            @endforeach
        @endif
        @if ($totbaln > 0)
                <tr>
                    <td colspan="4" class="text-right">Total Credit -</td>
                    <td colspan="3" class="text-left">{{$totbaln}}</td>
                </tr>
        @else
                <tr>
                    <td colspan="7" class="text-center font-red">No credit on this day</td>
                </tr>
        @endif
    </tbody>
</table>