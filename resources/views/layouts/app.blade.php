<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'System') }}</title>

    <link rel="shortcut icon" href="{{ asset('resources/assets/images/logoo.png') }}" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('resources/assets/global/plugins/datatables/datatables.min.css ') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css ') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('resources/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('resources/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('resources/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('resources/assets/layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('resources/assets/layouts/layout3/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ asset('resources/assets/layouts/layout3/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link href="{{ asset('resources/assets/global/plugins/typeahead/typeahead.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('resources/assets/pages/css/jquery-ui.css') }}">

    <link rel="shortcut icon" href="{{ asset('public/favicon.ico') }}" />
    <link href="{{ asset('resources/assets/pages/css/invoice-2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- select2 -->
    <link href="{{ asset('resources/assets/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('resources/assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/css/popup.css') }}">
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="{{ asset('javascript:;') }}" class="menu-toggler"></a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <script src="{{ asset('resources/assets/pages/scripts/form-input-mask.js') }}" type="text/javascript"></script>
</head>

<body class="page-container-bg-solid page-header-menu-fixed">
    <!-- BEGIN HEADER -->
    <div class="page-header">
        @if (!Auth::guest())
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler pull-left"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ route('/') }}">
                        <img src="{{ asset('resources/assets/images/logo.png') }}" style="width: 200px;" alt="logo" class="logo-default">
                    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user dropdown-dark" style="">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="{{ asset('resources/assets/layouts/layout3/img/avatar.png') }}">
                                <span class="username username-hide-mobile">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li><a href="{{ route('customer') }}"><i class="fa fa-user"></i> Customer </a></li>
                                <li><a href="{{ route('purchaser') }}"><i class="fa fa-user"></i> Purchaser </a></li>
                                <li class="divider"> </li>
                                <li><a href="{{ route('backup') }}"><i class="fa fa-external-link-square"></i> Backup </a></li>
                                <li><a href="{{ route('logout') }}"><i class="icon-key"></i> Log Out </a></li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER TOP -->
        <!-- BEGIN HEADER MENU -->
        <div class="page-header-menu">
            <div class="container">
                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="{{ route('home') }}"> Dashboard</a></li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Vehicle
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('vehicle') }}" class="nav-link  "> Add </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('vehicle.dates') }}" class="nav-link  "> Dates </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('vehicle.maintain') }}" class="nav-link  "> Maintances </a>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Materials
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('stock') }}" class="nav-link  "> Add </a>
                                </li>
                                <!-- <li class=" ">
                                    <a href="{{ route('sales.report') }}" class="nav-link  "> Report  </a>
                                </li>  -->
                            </ul>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Sales
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('sales') }}" class="nav-link  "> Add </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('sales.report') }}" class="nav-link  "> Report </a>
                                </li>

                                <li class=" ">
                                    <a href="{{ route('sales.creditlist') }}" class="nav-link  "> Credit Report </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('sales.daily_credit') }}" class="nav-link  "> Daily Credit Report </a>
                                </li>
                            </ul>
                        </li>


                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Purchase
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('purchase') }}" class="nav-link  "> Add </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('purchase.report') }}" class="nav-link  "> Report </a>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Loan
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('loan') }}" class="nav-link  "> Add </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('loan.report') }}" class="nav-link  "> Report </a>
                                </li>
                            </ul>
                        </li>

                        <li class=" "><a href="{{ route('expense') }}"> Expense </a></li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Payment
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('payment.sales') }}" class="nav-link  "> Sales </a>
                                </li>
                                <!-- <li class=" ">
                                    <a href="{{ route('payment.purchase') }}" class="nav-link  "> Purchase  </a>
                                </li> -->
                                <li class=" ">
                                    <a href="{{ route('payment.customer') }}" class="nav-link  "> Customer </a>
                                </li>
                                <!-- <li class=" ">
                                    <a href="{{ route('payment.purchaser') }}" class="nav-link  "> Purchaser  </a>
                                </li> -->
                                <li class=" ">
                                    <a href="{{ route('payment.old') }}" class="nav-link  "> OLD </a>
                                </li>
                            </ul>
                        </li>

                        <li class=" "><a href="{{ route('rental') }}"> Rental </a></li>
                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Report
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('dailyreport.daily') }}" class="nav-link  "> Daily Report </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('dailyreport.creport') }}" class="nav-link  "> Custom Days Report </a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Manage System
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="{{ route('account') }}" class="nav-link  "> Bill Repeat </a>
                                </li>
                                <li class=" ">
                                    <a href="{{ route('account.bill') }}" class="nav-link  "> Bill Number </a>
                                </li>
                            </ul>
                        </li> -->

                    </ul>
                </div>
                <!-- END MEGA MENU -->
            </div>
        </div>
        <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @yield('content')
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="container"> 2019 &copy; Chalavadakathil.
            <a href="https://www.saltu.in" title="Saltu Creative Suite" target="_blank">Saltu</a>
        </div>
    </div>
    <!-- END FOOTER -->
    <div class="scroll-to-top" id="non-printable">
        <i class="icon-arrow-up"></i>
    </div>
    @endif
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('resources/assets/pages/scripts/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/assets/pages/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('resources/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('resources/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- END CORE PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('resources/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('resources/assets/pages/scripts/form-samples.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('resources/assets/layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <!-- end sales -->
    <script src="{{ asset('resources/assets/pages/scripts/jquery.circliful.min.js') }}"></script>

    <script src="{{ asset('resources/assets/pages/scripts/jquery-ui.js') }}"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('resources/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('resources/assets/global/plugins/typeahead/handlebars.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('resources/assets/global/plugins/typeahead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- select2 -->
    <script src="{{ asset('resources/assets/js/select2.min.js') }}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('resources/assets/pages/scripts/components-typeahead.js') }}" type="text/javascript"></script>
    <!-- custom script -->
    <script src="{{ asset('resources/assets/js/script.js') }}" type="text/javascript"></script>
</body>

</html>