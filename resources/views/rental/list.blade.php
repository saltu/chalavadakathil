@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Rental
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Rental</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Rental Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{ route('rental.add') }}" method="POST" class="form-horizontal">            
                                                {{ csrf_field() }}                                   
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Descrption</label>
                                                        <div class="col-md-4">
                                                            <input type="text" required class="form-control input-circle" name="rdesc" id="rdesc" placeholder="Enter Description">
                                                        </div>
                                                    </div>                                                      
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Amount</label>
                                                        <div class="col-md-4">
                                                            <input type="number" required class="form-control input-circle" name="ramount" id="ramount" placeholder="Enter Amount">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('home') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Date </th>
                                        <th> Description </th>
                                        <th> Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($rentals) > 0 )
                                        @foreach ($rentals as $rental)
                                            <tr id="{{ $rental->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ date("d-m-Y",strtotime($rental->rdate ))}} </td>
                                                <td class="text-center"> {{ $rental->rdesc }} </td>
                                                <td class="text-center"> {{ $rental->ramount }} </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection