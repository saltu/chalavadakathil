 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> Title </th>

            <th class="text-center"> Bill No </th>
            <th class="text-center"> Holder Name </th>
            <th class="text-center"> Vehicle Number </th>
            <th class="text-center"> Material </th>
            <th class="text-center"> Amount Payed </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $op = 0;
            $stot = 0;
            $ptot = 0;
            $etot = 0;
            $lctot = 0;
            $ldtot = 0;
            $otot = 0;
            $rtot = 0;
        ?>
       <!-- opeinig balance -->
        @if(count($opening) > 0)
            @foreach($opening as $openin)
            <?php 
                $opdate = $openin->current_date;
                $opt = 0;
                $stott = 0;
                $ptott = 0;
                $etott = 0;
                $lctott = 0;
                $ldtott = 0;
                $otott = 0;
                $rtott = 0;
            ?>
            <tr class="warning">
                <td colspan="6" height="30px" class="text-center font-red sbold uppercase ">Date  {{ $openind = date("d-m-Y",strtotime($openin->current_date)) }}</td>
            <tr>
                <td> Opening Balance </td>
                <td colspan="4" class="text-center"> -- </td>
                <td class="text-center">{{ $openin->opening }}</td>
                <?php $opt = $openin->opening; ?>
            </tr>

        <!-- sales bill -->
        @if(count($sales_bills) > 0)
            @foreach($sales_bills as $sales_bill)
                @if( $opdate == $sales_bill->billdate )
                <tr>
                    <td> Sales {{ $sales_bill->desc }} </td>
                    <?php
                        $sales_billss = DB::table('sales_bills')
                                    ->select('*')
                                    ->where('bill_no',$sales_bill->billno)
                                    ->get();
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_billss[0]->custid)
                                    ->get();
                    ?>
                    <td> {{ $sales_bill->billno }} </td>
                    <td> {{ $customers[0]->name }} </td>
                    <td> {{ $sales_billss[0]->vehiclenum }} </td>
                    <td> {{ $sales_billss[0]->prodname }} </td>
                    <td class="text-center"> {{ $sales_bill->amount }} </td>
                    <?php $stot += $sales_bill->amount; 
                        $stott += $sales_bill->amount; ?>
                </tr>
                @endif
            @endforeach
        @endif

        <!-- sales customer -->
        @if(count($sales_bills_cus) > 0)
            @foreach($sales_bills_cus as $sales_bill_cus)
                @if( $opdate == $sales_bill_cus->billdate )
                <tr>
                    <td> Sales {{ $sales_bill_cus->desc }} </td>
                    <?php
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_bill_cus->billno)
                                    ->get();
                    ?>
                    <td colspan="4" class="text-center"> {{ $customers[0]->name }} </td>
                    <td class="text-center"> {{ $sales_bill_cus->amount }} </td>
                    <?php $stot += $sales_bill_cus->amount;
                    $stott += $sales_bill_cus->amount; ?>
                </tr>
                @endif
            @endforeach
        @endif
        <!-- purchase bill-->
       @if(count($purchase_bills) > 0)
            @foreach($purchase_bills as $purchase_bill)
                @if( $opdate == $purchase_bill->billdate )
                <tr>
                    <td> Purchase {{ $purchase_bill->desc }}</td>
                    <?php 
                        $purchase_billss = DB::table('purchase_bills')
                                    ->select('*')
                                    ->where('bill_no',$purchase_bill->billno)
                                    ->get();
                        $purchasers = DB::table('purchasers')
                                    ->select('*')
                                    ->where('id',$purchase_billss[0]->purid)
                                    ->get();

                    ?>
                    <td> {{ $purchase_bill->billno }} </td>
                    <td> {{ $purchasers[0]->name }} </td>
                    <td> {{ $purchase_billss[0]->vehiclenum }} </td>
                    <td> {{ $purchase_billss[0]->prodname }}(CFT- {{ $purchase_billss[0]->quantity }}) </td>
                    <td class="text-center"> <!-- {{ $purchase_bill->amount }} --> </td>
                </tr>
                @endif
            @endforeach
        @endif
        <!-- expense -->
        @if(count($expenses) > 0)
            @foreach($expenses as $expense)
                @if( $opdate == $expense->ex_date )
                <tr>
                    <td> Expense </td>
                    <td colspan="4" class="text-center"> {{ $expense->description }} </td>
                    <td class="text-center"> {{ $expense->amount }} </td>
                    <?php $etot += $expense->amount;
                    $etott += $expense->amount; ?>
                </tr>
                @endif
            @endforeach
        @endif 
         <!-- loan Credited-->
        @if(count($loan_details_cr) > 0)
            @foreach($loan_details_cr as $loan_detail_cr)
                @if( $opdate == $loan_detail_cr->lddate )
                <tr>
                    <td> Loan Credited</td>
                    <?php 
                        $loans_cr = DB::table('loans')
                                    ->select('*')
                                    ->where('id',$loan_detail_cr->source_id)
                                    ->get();

                    ?>
                    <td colspan="4" class="text-center"> {{  $loans_cr[0]->source}} </td>
                    <td class="text-center"> {{ $loan_detail_cr->amount }} </td>
                    <?php $lctot += $loan_detail_cr->amount;
                    $lctott += $loan_detail_cr->amount; ?>
                </tr>
                @endif
            @endforeach
        @endif 
        <!-- loan Debited-->
        @if(count($loan_details_dr) > 0)
            @foreach($loan_details_dr as $loan_detail_dr)
             @if( $opdate == $loan_detail_dr->lddate )
                <tr>
                    <td> Loan Debited</td>
                     <?php 
                        $loans_dr = DB::table('loans')
                                    ->select('*')
                                    ->where('id',$loan_detail_dr->source_id)
                                    ->get();

                    ?>
                    <td colspan="4" class="text-center"> {{ $loans_dr[0]->source }} </td>
                    <td class="text-center"> {{ $loan_detail_dr->amount }} </td>
                    <?php $ldtot += $loan_detail_dr->amount;
                    $ldtott += $loan_detail_dr->amount; ?>
                </tr>
            @endif
            @endforeach
        @endif

        <!-- Old Payment -->
        @if(count($old_payments) > 0)
            @foreach($old_payments as $old_payment)
            @if( $opdate == $old_payment->odate )
                <tr>
                    <td> Old Payment</td>
                    <td> Income</td>
                    <td colspan="2" class="text-center"> {{ $old_payment->oname }} </td>
                    <td class="text-center"> {{ $old_payment->oitem }} </td>
                    <td class="text-center"> {{ $old_payment->oamount }} </td>
                    <?php $otot += $old_payment->oamount;
                    $otott += $old_payment->oamount; ?>
                </tr>
                @endif
            @endforeach
        @endif

        <!-- Rental -->
        @if(count($rentals) > 0)
            @foreach($rentals as $rental)
                @if( $opdate == $rental->rdate )
                <tr>
                    <td> Rental</td>
                    <td> Income</td>
                    <td colspan="3" class="text-center"> {{ $rental->rdesc }} </td>
                    <td class="text-center"> {{ $rental->ramount }} </td>
                    <?php $rtot += $rental->ramount;
                    $rtott += $rental->ramount; ?>
                </tr>
                @endif
            @endforeach
        @endif

        <!-- stock details -->
        <tr class="success">
            <td colspan="6" class="text-center">Stock Details</td>
        </tr>
        <tr class="info">
            <td colspan="4" class="text-center">Material Name</td>
            <td class="text-center">Opening</td>
            <td class="text-center">Closing</td>
        </tr>
        @if(count($stock_details) > 0)
            @foreach($stock_details as $stock_detail)
                @if( $opdate == $stock_detail->cudate )
                                <tr>
                    <?php  $material = DB::table('products')
                                    ->select('*')
                                    ->where('id',$stock_detail->material_id)
                                    ->get();
                    ?>
                    <td colspan="4" class="text-center"> {{ $material[0]->name }} </td>
                    <td class="text-center"> {{ $stock_detail->cft }} </td>
                    <?php $stockcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($stock_detail->cudate)). " +1 day"));
                            $cdate = date("Y-m-d");
                            $closings = DB::table('stock_details')
                                    ->select('*')
                                    ->where('cudate',$stockcdate)
                                    ->where('material_id',$stock_detail->material_id)
                                    ->get();
                    ?>

                    @if( $stock_detail->cudate == $cdate )
                    <?php  $curstock = DB::table('products')
                                    ->select('*')
                                    ->where('id',$stock_detail->material_id)
                                    ->get();
                    ?>
                    <td class="text-center"> {{ $curstock[0]->cft }} </td>
                    @endif
                    <?php 
                    if(count($closings) == 0 && $stock_detail->cudate != $cdate )
                    {
                        do{
                            $stockcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($stockcdate)). " +1 day"));
                            $closings = DB::table('stock_details')
                                    ->select('*')
                                    ->where('cudate',$stockcdate)
                                    ->where('material_id',$stock_detail->material_id)
                                    ->get();
                                }while ( count($closings) == 0 );
                    ?>
                    <td class="text-center"> {{ $closings[0]->cft }} </td>
                    <?php }else{ ?>
                    @if( $stock_detail->cudate != $cdate )<td class="text-center"> {{ $closings[0]->cft }} </td>@endif
                    <?php } ?>
                </tr>
                @endif
            @endforeach
        @endif

        <tr>
            <?php 
            $dtot = ($opt + $stott + $lctott + $otott + $rtott) - ($ptott + $etott + $ldtott);
            ?>
            <td>Clossing Balance</td>
            <td colspan="4" class="text-center">--</td>
            <td class="text-center">{{ $dtot }}</td>
        </tr>
        <tr>
            <td colspan="6"></td>
        </tr>
            @endforeach
        @endif 
        <tr>
            <td colspan="3" class="text-right">Sales Total</td>
            <td colspan="3">{{ $stot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Expense Total</td>
            <td colspan="3">{{ $etot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Loan Credited Total</td>
            <td colspan="3">{{ $lctot }}</td>
        </tr>

        <tr>
            <td colspan="3" class="text-right">Loan Debited Total</td>
            <td colspan="3">{{ $ldtot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Old Payment Total</td>
            <td colspan="3">{{ $otot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Rental Total</td>
            <td colspan="3">{{ $rtot }}</td>
        </tr>
        <tr>
            <?php 
            $tot = ($op + $stot + $lctot + $otot + $rtot) - ($ptot + $etot + $ldtot);
            ?>
            <td colspan="3" class="text-right">Account Balance</td>
            <td colspan="3">{{ $dtot }}</td>
        </tr>

    </tbody>
</table>
