<tr class="success">
            <td colspan="6" class="text-center">Stock Details</td>
        </tr>
        <tr class="info">
            <td colspan="4" class="text-center">Material Name</td>
            <td class="text-center">Opening</td>
            <td class="text-center">Closing</td>
        </tr>
        @if(count($stock_details) > 0)
            @foreach($stock_details as $stock_detail)
                <tr>
                    <?php  $material = DB::table('products')
                                    ->select('*')
                                    ->where('id',$stock_detail->material_id)
                                    ->get();
                    ?>
                    <td colspan="4" class="text-center"> {{ $material[0]->name }} </td>
                    <td class="text-center"> {{ $stock_detail->cft }} </td>
                    <?php $stockcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($stock_detail->cudate)). " +1 day"));
                            $cdate = date("Y-m-d");
                            $closings = DB::table('stock_details')
                                    ->select('*')
                                    ->where('cudate',$stockcdate)
                                    ->where('material_id',$stock_detail->material_id)
                                    ->get();
                    ?>

                    @if( $stock_detail->cudate == $cdate )
                    <?php  $curstock = DB::table('products')
                                    ->select('*')
                                    ->where('id',$stock_detail->material_id)
                                    ->get();
                    ?>
                    <td class="text-center"> {{ $curstock[0]->cft }} </td>
                    @endif
                    <?php 
                    if(count($closings) == 0 && $stock_detail->cudate != $cdate )
                    {
                        do{
                            $stockcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($stockcdate)). " +1 day"));
                            $closings = DB::table('stock_details')
                                    ->select('*')
                                    ->where('cudate',$stockcdate)
                                    ->where('material_id',$stock_detail->material_id)
                                    ->get();
                                }while ( count($closings) == 0 );
                    ?>
                    <td class="text-center"> {{ $closings[0]->cft }} </td>
                    <?php }else{ ?>
                    @if( $stock_detail->cudate != $cdate )<td class="text-center"> {{ $closings[0]->cft }} </td>@endif
                    <?php } ?>
                </tr>
            @endforeach
        @endif