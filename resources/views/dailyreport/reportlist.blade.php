 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> Title </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Holder Name </th>
            <th class="text-center"> Vehicle Number </th>
            <th class="text-center"> Material </th>
            <th class="text-center"> Bill Amount </th>
            <th class="text-center"> Amount Payed </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $stot = 0;
            $ptot = 0;
            $etot = 0;
            $lctot = 0;
            $ldtot = 0;
            $otot = 0;
            $rtot = 0;
            $tot = 0;
        ?>
       <!-- opeinig balance -->
        @if(count($opening) > 0)
        <tr>
            <td> Opening Balance </td>
            <td colspan="4" class="text-center"> -- </td>
            <td colspan="2" class="text-center">{{ $opening[0]->opening }}</td>
        </tr>
        @endif 
        <!-- sales bill -->
        @if(count($sales_bills) > 0)
            @foreach($sales_bills as $sales_bill)
                <tr>
                    <td> Sales {{ $sales_bill->desc }} </td>
                    <?php
                        $sales_billss = DB::table('sales_bills')
                                    ->select('*')
                                    ->where('bill_no',$sales_bill->billno)
                                    ->get();
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_billss[0]->custid)
                                    ->get();
                    ?>
                    <td> {{ $sales_bill->billno }} </td>
                    <td> {{ $customers[0]->name }} </td>
                    <td> {{ $sales_billss[0]->vehiclenum }} </td>
                    <td> {{ $sales_billss[0]->prodname }} </td>
                    <td> {{ $sales_billss[0]->total }} </td>
                    <td> {{ $sales_bill->amount }} </td>
                    <?php $stot += $sales_bill->amount; ?>
                </tr>
            @endforeach
        @endif

        <!-- sales customer -->
        @if(count($sales_bills_cus) > 0)
            @foreach($sales_bills_cus as $sales_bill_cus)
                <tr>
                    <td> Sales {{ $sales_bill_cus->desc }} </td>
                    <?php
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_bill_cus->billno)
                                    ->get();
                    ?>
                    <td colspan="5" class="text-center"> {{ $customers[0]->name }} </td>
                    <td colspan="1" class="text-left"> {{ $sales_bill_cus->amount }} </td>
                    <?php $stot += $sales_bill_cus->amount; ?>
                </tr>
            @endforeach
        @endif
        <!-- purchase bill-->
        @if(count($purchase_bills) > 0)
            @foreach($purchase_bills as $purchase_bill)
                <tr>
                    <td> Purchase {{ $purchase_bill->desc }}</td>
                    <?php 
                        $purchase_billss = DB::table('purchase_bills')
                                    ->select('*')
                                    ->where('bill_no',$purchase_bill->billno)
                                    ->get();
                        $purchasers = DB::table('purchasers')
                                    ->select('*')
                                    ->where('id',$purchase_billss[0]->purid)
                                    ->get();

                    ?>
                    <td> {{ $purchase_bill->billno }} </td>
                    <td> {{ $purchasers[0]->name }} </td>
                    <td> {{ $purchase_billss[0]->vehiclenum }} </td>
                    <td> {{ $purchase_billss[0]->prodname }}(CFT- {{ $purchase_billss[0]->quantity }}) </td>
                    <td>  </td>
                    <td>  </td>
                </tr>
            @endforeach
        @endif
        <!-- expense -->
        @if(count($expenses) > 0)
            @foreach($expenses as $expense)
                <tr>
                    <td> Expense </td>
                    <td colspan="3" class="text-center"> {{ $expense->description }} </td>
                    <td colspan="3" class="text-center"> {{ $expense->amount }} </td>
                    <?php $etot += $expense->amount; ?>
                </tr>
            @endforeach
        @endif 

        <!-- loan Credited-->
        @if(count($loan_details_cr) > 0)
            @foreach($loan_details_cr as $loan_detail_cr)
                <tr>
                    <td> Loan Credited</td>
                    <?php 
                        $loans_cr = DB::table('loans')
                                    ->select('*')
                                    ->where('id',$loan_detail_cr->source_id)
                                    ->get();

                    ?>
                    <td colspan="5" class="text-center"> {{  $loans_cr[0]->source}} </td>
                    <td colspan="1" class="text-left"> {{ $loan_detail_cr->amount }} </td>
                    <?php $lctot += $loan_detail_cr->amount; ?>
                </tr>
            @endforeach
        @endif 
        <!-- loan Debited-->
        @if(count($loan_details_dr) > 0)
            @foreach($loan_details_dr as $loan_detail_dr)
                <tr>
                    <td> Loan Debited</td>
                     <?php 
                        $loans_dr = DB::table('loans')
                                    ->select('*')
                                    ->where('id',$loan_detail_dr->source_id)
                                    ->get();

                    ?>
                    <td colspan="5" class="text-center"> {{ $loans_dr[0]->source }} </td>
                    <td colspan="1" class="text-left"> {{ $loan_detail_dr->amount }} </td>
                    <?php $ldtot += $loan_detail_dr->amount; ?>
                </tr>
            @endforeach
        @endif

        <!-- Rental -->
        @if(count($rentals) > 0)
            @foreach($rentals as $rental)
                <tr>
                    <td> Rental</td>
                    <td colspan="3"> Income</td>
                    <td colspan="2" class="text-left"> {{ $rental->rdesc }} </td>
                    <td  class="text-left"> {{ $rental->ramount }} </td>
                    <?php $rtot += $rental->ramount; ?>
                </tr>
            @endforeach
        @endif

        <!-- Old Payment -->
        @if(count($old_payments) > 0)
            @foreach($old_payments as $old_payment)
                <tr>
                    <td> Old Payment</td>
                    <td> Income</td>
                    <td colspan="2" class="text-left"> {{ $old_payment->oname }} </td>
                    <td colspan="2" class="text-left"> {{ $old_payment->oitem }} </td>
                    <td> {{ $old_payment->oamount }} </td>
                    <?php $otot += $old_payment->oamount; ?>
                </tr>
            @endforeach
        @endif


        <!-- stock details -->
        


        <tr>
            <td colspan="3" class="text-right">Opening Balance</td>
            <td colspan="4">{{ $opening[0]->opening }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Sales Total</td>
            <td colspan="4">{{ $stot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Expense Total</td>
            <td colspan="4">{{ $etot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Loan Credited Total</td>
            <td colspan="4">{{ $lctot }}</td>
        </tr>

        <tr>
            <td colspan="3" class="text-right">Loan Debited Total</td>
            <td colspan="4">{{ $ldtot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Old Payment Total</td>
            <td colspan="4">{{ $otot }}</td>
        </tr>
        <tr>
            <td colspan="3" class="text-right">Rental Total</td>
            <td colspan="4">{{ $rtot }}</td>
        </tr>
        <tr>
            <?php 
            $op = $opening[0]->opening;
            $tot = ($op + $stot + $lctot + $otot + $rtot) - ($ptot + $etot + $ldtot);
            ?>
            <td colspan="3" class="text-right">Account Balance</td>
            <td colspan="4">{{ $tot }}</td>
        </tr>

    </tbody>
</table>