@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Manage Account</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Manage Account</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Bill Repeat</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <h3 class="form-section">Info</h3>
                                        <div class="row">
                                            
                                            <div class="col-md-2"></div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <button class="btn" name="search_sub" id="search_sub"><i class="fa fa-search" aria-hidden="true"></i> Click to search Repeat Bills</button>                                           
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <h3 class="form-section display-hide" id="search_sub1">Bills Repeated Today -- <?php echo date('d-m-Y'); ?></h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="portlet-body">
                                                    <div class="search_sub2" id="search_sub2"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> 
                                                <?php 
                                                $bill_no = Session::get('bill_no');
                                                if(!empty(Session::get('bill_no'))){
                                                ?>
                                                <form action="{{ route('account.bill.delete') }}" method="post">   {{ csrf_field() }} 
                                                    <input type="hidden" name="bill_no" id="bill_no" value="<?php echo $bill_no; ?>">       
                                                    <button class="btn-danger display-hide" name="delete_bill" id="delete_bill">Delete 1 entery of bill </button> 
                                                </form>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <!-- /row -->
                                         <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection