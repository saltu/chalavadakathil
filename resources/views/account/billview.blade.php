@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Manage Account</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Manage Account</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Bill Number</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <h3 class="form-section">Info</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form action="{{ route('account.bill.update') }}" method="post">    {{ csrf_field() }}                     
                                                    <label class="col-md-3">Sales Bill Number</label>
                                                    <?php 
                                                    $invoice_nums = DB::table('invoice_nums')->select('*')->where('id',1)->get();
                                                    ?>
                                                    <div class="col-md-3">
                                                       <input type="text" required class="form-control bill_number_up" value="{{ $invoice_nums[0]->invoice_num }}" id="bill_number_up" name="bill_number_up" >
                                                    </div>
                                                    <div class="col-md-3">
                                                       <button class="btn-success" type="submit">Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!--/row-->
                                         <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection