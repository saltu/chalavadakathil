<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr class="d-flex">
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Total Amount </th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $sales_bill_data = DB::table('sales_bills')->select('*')->where('bill_no',$bill_no)->get();
        ?>
        @if(count($sales_bill_data) > 0)
        @foreach($sales_bill_data as $sales_bill)
        <tr>
            <td class="text-center"> {{  $sales_bill->bill_no }} </td>
            <td class="text-center"> {{  $sales_bill->total }} </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>