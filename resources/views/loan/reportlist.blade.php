 <!-- datatables scripts -->
<script src="{{ asset('resources/assets/pages/scripts/table-datatables-buttons.js') }}" type="text/javascript"></script>
<table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
        <tr class="d-flex" >
            <th> # </th>
            <th class="text-center"> Loan Date </th>
            <th class="text-center"> Amount Payed </th>
            <th class ="text-center col-md-2"> Payment Type </th>
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        @if(count($loan_data_details) > 0)
            @foreach($loan_data_details as $loan_data_detail)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $lddate=date("d-m-Y",strtotime($loan_data_detail->lddate)) }} </td>
                    <td> {{ $loan_data_detail->amount }} </td>
                    <td> {{ $loan_data_detail->type }} </td>
                </tr>
            @endforeach
        @endif 
    </tbody>
</table>
<?php 

$loan_datas = DB::table('loans')
                ->select('*')
                ->where('id',$loan_data_details[0]->source_id)
                ->get();
?>
<br><br>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3">
            <label class="control-label col-md-4">Total Amount</label>
           <input type="text" readonly class="form-control tamount" value="{{ $loan_datas[0]->lamount }}" id="tamount" name="tamount" placeholder="Enter Loan Source Name">
        </div>
        <div class="col-md-4">
            <label class="control-label col-md-4">Amount Given</label>
           <input type="text" readonly class="form-control gamount" id="gamount" value="{{ $loan_datas[0]->pamount }}" name="gamount" placeholder="Enter Loan Source Name">
        </div>
        <div class="col-md-4">
            <label class="control-label col-md-4">Balance Amount</label>
           <input type="text" readonly class="form-control bamount" id="bamount" name="bamount" value="{{ $loan_datas[0]->lamount - $loan_datas[0]->pamount }}" placeholder="Enter Loan Source Name">
        </div>
    </div>
</div>