@extends('layouts.app')

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Dashboard</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-briefcase fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $tot_sales = 0;
                                    $accounts1 = DB::table('sales_payments')
                                                    ->select('*')
                                                    ->where('billdate',date('Y-m-d'))
                                                    ->get();
                                    foreach ($accounts1 as $key => $value) {
                                        $tot_sales += $value->amount;
                                    }
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $tot_sales }}</div>
                                    <div class="desc">Today's Sales </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-group fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $tot_expense= 0;
                                    $accounts3 = DB::table('expenses')
                                                    ->select('*')
                                                    ->where('ex_date',date('Y-m-d'))
                                                    ->get();
                                        foreach ($accounts3 as $key => $value) {
                                        $tot_expense += $value->amount;
                                    }
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $tot_expense }}</div>
                                    <div class="desc">Today's Expense </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow" >
                                <div class="visual">
                                    <i class="fa fa-anchor fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts5 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',5)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $accounts5[0]->amount }}</div>
                                    <div class="desc">Total Sales Credit</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat ngreen" >
                                <div class="visual opacity">
                                    <i class="fa fa-credit-card fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts7 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',7)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $accounts7[0]->amount }}</div>
                                    <div class="desc">Total Loan Credit </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat dpink" >
                                <div class="visual opacity">
                                    <i class="fa fa-pie-chart fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts8 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',8)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $accounts8[0]->amount }}</div>
                                    <div class="desc">Total Loan Debit </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple" >
                                <div class="visual">
                                    <i class="fa fa-balance-scale fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $accounts4 = DB::table('accounts')
                                                    ->select('*')
                                                    ->where('id',1)
                                                    ->get();
                                    ?>
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> {{ $accounts4[0]->amount }}</div>
                                    <div class="desc"> Account Balance </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="myBtn">
                            <div class="dashboard-stat lgreen" >
                                <div class="visual opacity">
                                    <i class="fa fa fa-flag fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    <?php
                                    $notf = DB::table('notifications')
                                                    ->select('*')
                                                    ->where('view',0)
                                                    ->get();
                                    ?>
                                    <div class="number">Click Here <i class="fa fa-bell fa-icon-small" aria-hidden="true"></i>
                                        @if (count($notf) > 0)
                                            {{count($notf)}} new 
                                        @endif
                                    </div>
                                    <div class="desc">Vehicle Notification</div>
                                </div>
                            </div>
                        </div>
                    </div> 
    <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <?php 
            $datefrom = date('Y-m-d');
            $dateto = date("Y-m-d",strtotime(date("Y-m-d",strtotime($datefrom)). " +1 week"));
            $exp_notify = DB::table ('vehicles')
                        ->select ('*')
                        ->where ('ownername','=','chalavadakathil' )
                        ->where('permit','<',$datefrom)
                        ->orWhere('tax','<',$datefrom)
                        ->orWhere('test','<',$datefrom)
                        ->orWhere('insurance','<',$datefrom)
                        ->orderBy('id','DESC')
                        ->get();
            $permitnotify = DB::table ('vehicles')
                        ->select ('*')
                        ->where ('ownername','=','chalavadakathil' )
                        ->whereBetween('permit',[$datefrom,$dateto])
                        ->orderBy('id','DESC')
                        ->get();
            $taxnotify = DB::table ('vehicles')
                        ->select ('*')
                        ->where ('ownername','=','chalavadakathil' )
                        ->whereBetween('tax',[$datefrom,$dateto])
                        ->orderBy('id','DESC')
                        ->get();
            $testnotify = DB::table ('vehicles')
                        ->select ('*')
                        ->where ('ownername','=','chalavadakathil' )
                        ->whereBetween('test',[$datefrom,$dateto])
                        ->orderBy('id','DESC')
                        ->get();
            $insurancenotify = DB::table ('vehicles')
                        ->select ('*')
                        ->where ('ownername','=','chalavadakathil' )
                        ->whereBetween('insurance',[$datefrom,$dateto])
                        ->orderBy('id','DESC')
                        ->get();
            $notify = DB::table ('notifications')
                        ->select ('*')
                        ->where('view',0)
                        ->get();
            $notify_o = DB::table ('notifications')
                        ->select ('*')
                        ->where('view',1)
                        ->get();
        ?>
        
        <span class="close">&times;</span>
        <span class="caption-subject font-red sbold uppercase">Notifications</span><br>
        <span class="caption ml-5 btn btn-success im">Notifications </span>
        <span class="caption ml-5 btn btn-success 0">Expired Vehicles </span>
        <span class="caption ml-5 btn btn-warning 1">Permit</span> 
        <span class="caption ml-5 btn btn-warning 2">Tax</span> 
        <span class="caption ml-5 btn btn-warning 3">Test</span> 
        <span class="caption ml-5 btn btn-warning 4">Insurance</span>
        <br><br><br>

        <div class="portlet-body" id="samplef">                            
        <table class="table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Permit Date </th>
                    <th class="text-center"> Tax Date </th>
                    <th class="text-center"> Test Date </th>
                    <th class="text-center"> Insurance Date </th>
                    <!-- <th class="text-center"> Edit </th> -->
                </tr>
            </thead>
            <tbody>
            <?php $i = 1;
                $j = 1; ?>
            @if(count($notify) > 0 )
            <h4 class="font-red">New Notifications</h4>
                @foreach ($notify as $notifyy)
                <tr class="warning" id="{{ $notifyy->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notifyy->vehicle_id }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notifyy->epermit)) }} </td>
                    <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($notifyy->etax)) }} </td>
                    <td class="text-center"> {{ $test=date("d-m-Y", strtotime($notifyy->etest)) }} </td>
                    <td class="text-center"> {{ $insurance=date("d-m-Y", strtotime($notifyy->einsurance)) }} </td>
                    <!-- <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notifyy->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td> -->
                </tr>
                @endforeach
            @endif
            @if(count($notify_o) > 0 )
                <tr class="font-blue"><td class="text-center" colspan="6">Upcoming Renewals</td></tr>
                @foreach ($notify_o as $notifyy_o)
                <tr id="{{ $notifyy_o->id }}">
                    <td class="text-center"> {{ $j++ }} </td>
                    <td class="text-center"> {{ $notifyy_o->vehicle_id }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notifyy_o->epermit)) }} </td>
                    <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($notifyy_o->etax)) }} </td>
                    <td class="text-center"> {{ $test=date("d-m-Y", strtotime($notifyy_o->etest)) }} </td>
                    <td class="text-center"> {{ $insurance=date("d-m-Y", strtotime($notifyy_o->einsurance)) }} </td>
                    <!-- <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notifyy_o->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td> -->
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
        <div class="portlet-body display-hide" id="sample0">                            
        <table class="table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Permit Date </th>
                    <th class="text-center"> Tax Date </th>
                    <th class="text-center"> Test Date </th>
                    <th class="text-center"> Insurance Date </th>
                    <!-- <th class="text-center"> Edit </th> -->
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($exp_notify) > 0 )
                @foreach ($exp_notify as $notifyy)
                <tr id="{{ $notifyy->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notifyy->number }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notifyy->permit)) }} </td>
                    <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($notifyy->tax)) }} </td>
                    <td class="text-center"> {{ $test=date("d-m-Y", strtotime($notifyy->test)) }} </td>
                    <td class="text-center"> {{ $insurance=date("d-m-Y", strtotime($notifyy->insurance)) }} </td>
                    <!-- <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notifyy->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td> -->
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
        <div class="portlet-body display-hide" id="sample1">                            
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Permit Validity Date </th>
                    <th class="text-center"> Edit </th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($permitnotify) > 0 )
                @foreach ($permitnotify as $notify)
                <tr id="{{ $notify->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notify->number }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notify->permit)) }} </td>
                    <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notify->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        </div>
        <div class="portlet-body display-hide" id="sample2">                            
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Tax Validity Date </th>
                    <th class="text-center"> Edit </th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($taxnotify) > 0 )
                @foreach ($taxnotify as $notify)
                <tr id="{{ $notify->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notify->number }}</td>
                    <td class="text-center"> {{ $tax=date("d-m-Y", strtotime($notify->tax)) }} </td>
                    <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notify->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div class="portlet-body  display-hide" id="sample3">                            
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Re-Test Date </th>
                    <th class="text-center"> Edit </th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($testnotify) > 0 )
                @foreach ($testnotify as $notify)
                <tr id="{{ $notify->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notify->number }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notify->test)) }} </td>
                    <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notify->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div class="portlet-body  display-hide" id="sample4">                            
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="text-center"> Sl.No </th>
                    <th class="text-center"> Vehicle Number </th>
                    <th class="text-center"> Insurance Validity Date </th>
                    <th class="text-center"> Edit </th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @if(count($insurancenotify) > 0 )
                @foreach ($insurancenotify as $notify)
                <tr id="{{ $notify->id }}">
                    <td class="text-center"> {{ $i++ }} </td>
                    <td class="text-center"> {{ $notify->number }}</td>
                    <td class="text-center"> {{ $permit=date("d-m-Y", strtotime($notify->insurance)) }} </td>
                    <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notify->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
</div>
<!-- END CONTENT -->
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
</div>
    <!-- END PAGE CONTENT BODY -->
    <!-- END CONTENT BODY -->

@endsection